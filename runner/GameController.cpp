#include "stdafx.h"
#include "constants.h"
#include "SpriteFile.h"
#include "ResourceHandler.h"
#include "TextBox.h"
#include "GameMenu.h"
#include "Animation.h"
#include "TmxLevel.h"
#include "ContactHandler.h"
#include "Bonus.h"
#include "Platform.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Runner.h"
#include "GameController.h"

CGameController::CGameController()
    : m_state(AppState::Loading)
    , m_runner(m_assets)
{
    OpenWindow();
    InitializeMenu();

    Render();

    m_assets.musics.at(Music::Menu).play();
    m_assets.images.at(Image::GameBackground).ResizeToTargetSize(m_window);
    m_assets.images.at(Image::MenuBackground).ResizeToTargetSize(m_window);

    m_state = AppState::InMenu;
}

void CGameController::EnterGameLoop()
{
    while (m_window.isOpen())
    {
        HandleEvents();
        Update();
        Render();
    }
}

void CGameController::HandleEvents()
{
    sf::Event event;

    while (m_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_window.close();
        }
        else if (event.type == sf::Event::Resized)
        {
            ProcessWindowResizing(event);
        }

        if (m_state == AppState::InMenu)
        {
            ProcessMenuUserInputEvents(event);
        }
        else if (m_state == AppState::Playing)
        {
            ProcessGameUserInputEvents(event);
        }
    }
}

void CGameController::Update()
{
    m_elapsedTime = m_clock.restart().asSeconds();
    m_elapsedTime = (m_elapsedTime >= 0.1f) ? 0.1f : m_elapsedTime;

    if (m_state == AppState::Playing)
    {
        UpdateRunner();
        UpdatePlatforms();
        UpdateBonuses();
        UpdateEnemies();
        UpdateTextBoxes();
    }
}

void CGameController::ProcessCollisions(IContactHandler &entity, const Axis axis)
{
    for (const auto &tmxObject : m_tmxObjects)
    {
        if (entity.ToFloatRect().intersects(tmxObject.rect))
        {
            if (STATIC_CATEGORY_MAPPING.find(tmxObject.name) != STATIC_CATEGORY_MAPPING.end())
            {
                StaticCategory category = STATIC_CATEGORY_MAPPING.at(tmxObject.name);
                entity.OnContactStatic(axis, category, tmxObject.rect);
                if (HandleStaticIntersection(entity, category))
                {
                    break;
                }
            }
        }
    }
}

bool CGameController::HandleStaticIntersection(IContactHandler &entity, StaticCategory category)
{
    bool handled = false;

    if (entity.GetType() == Entity::Runner)
    {
        switch (category)
        {
        case StaticCategory::NextLevel:
            m_state = AppState::Loading;
            Render();
            LevelRawMemoryRealloc(++m_levelNo);
            InitializeNewGame();
            handled = true;
            break;
        case StaticCategory::EndGame:
            m_assets.musics.at(Music::Game).pause();
            m_assets.musics.at(Music::Menu).play();
            SetScreenViewToDefault();
            m_menu.InitializeOnGameWin(m_window, m_runner.GetScore());
            m_state = AppState::InMenu;
            handled = true;
            break;
        default:
            break;
        }
    }

    return handled;
}

void CGameController::UpdateRunner()
{
    m_runner.Update(m_elapsedTime);

    m_runner.MoveOnStep(Axis::X, m_elapsedTime);
    ProcessCollisions(m_runner, Axis::X);

    m_runner.MoveOnStep(Axis::Y, m_elapsedTime);
    ProcessCollisions(m_runner, Axis::Y);

    if (!m_runner.IsAlive())
    {
        m_assets.sounds.at(Sound::RunnerDead).Play();
        m_assets.musics.at(Music::Game).stop();
        m_menu.ReinitializeAfterGameLoss(m_window, m_runner.GetScore());
        m_state = AppState::InMenu;
    }
}

void CGameController::Render()
{
    SetScreenViewToDefault();

    if (m_state == AppState::Playing)
    {
        m_assets.images.at(Image::GameBackground).Draw(m_window);
    }
    else if (m_state == AppState::InMenu)
    {
        m_assets.images.at(Image::MenuBackground).Draw(m_window);
    }

    if (m_state == AppState::Playing)
    {
        MoveScreenViewByRunnerPosition();
        m_level->Draw(m_window);
        DrawPlatforms();
        DrawBonuses();
        DrawEnemies();
        DrawTextBoxes();
        m_runner.Draw(m_window);
        m_menu.DrawGameInterface(m_window, m_runner.GetScore(), m_runner.GetHealth());
    }
    else if (m_state == AppState::InMenu)
    {
        m_menu.Draw(m_window);
    }

    if (m_menu.IsFpsEnabled())
    {
        m_menu.DrawFps(m_window, GetFps());
    }

    if ((m_state == AppState::InMenu) || (m_state == AppState::Playing))
    {
        SetScreenViewToDefault();
        m_assets.images.at(Image::Cursor).Draw(m_window);
    }

    if (m_state == AppState::Loading)
    {
        m_menu.DrawLoadingScreen(m_window);
    }

    m_window.display();
}

void CGameController::ProcessMenuUserInputEvents(const sf::Event &event)
{
    if (event.type == sf::Event::KeyPressed)
    {
        switch (event.key.code)
        {
        case sf::Keyboard::Up:
            m_assets.sounds.at(Sound::MouseOver).Play();
            m_menu.MoveUp();
            break;
        case sf::Keyboard::Down:
            m_assets.sounds.at(Sound::MouseOver).Play();
            m_menu.MoveDown();
            break;
        case sf::Keyboard::Return:
            m_assets.sounds.at(Sound::MouseClick).Play();
            ProcessItemPressInMenu();
            break;
        case sf::Keyboard::Escape:
            if (m_menu.IsGameOnPause())
            {
                m_assets.sounds.at(Sound::MenuClose).Play();
                m_assets.musics.at(Music::Game).play();
                m_assets.musics.at(Music::Menu).pause();
                m_state = AppState::Playing;
            }
            break;
        default:
            break;
        }
    }

    if (event.type == sf::Event::MouseMoved)
    {
        UpdateCursor(event);

        for (unsigned i = 0; i < m_menu.GetItemsAmount(); ++i)
        {
            if (m_menu.GetItemAsRect(i).contains(sf::Mouse::getPosition(m_window)))
            {
                if (!m_menu.IsHovered(i))
                {
                    m_assets.sounds.at(Sound::MouseOver).Play();
                }

                m_menu.SetCurrentItem(i);
            }
        }
    }

    if (event.type == sf::Event::MouseButtonPressed)
    {
        if (event.mouseButton.button == sf::Mouse::Left)
        {
            for (unsigned i = 0; i < m_menu.GetItemsAmount(); ++i)
            {
                if (m_menu.GetItemAsRect(i).contains(sf::Mouse::getPosition(m_window)))
                {
                    m_assets.sounds.at(Sound::MouseClick).Play();
                    ProcessItemPressInMenu();
                }
            }
        }
    }
}

void CGameController::ProcessItemPressInMenu()
{
    switch (m_menu.GetPressedItem())
    {
    case MenuItem::Start:
        if ((m_levelNo == 1) && (m_levelWasLoaded))
        {
            InitializeNewGame();
            m_assets.musics.at(Music::Menu).pause();
            m_assets.musics.at(Music::Game).stop();
            m_assets.musics.at(Music::Game).play();
        }
        else
        {
            m_levelNo = 1;
            m_state = AppState::Loading;
            Render();
            LevelRawMemoryRealloc(m_levelNo);
            InitializeNewGame();
            m_assets.musics.at(Music::Menu).pause();
            m_assets.musics.at(Music::Game).stop();
            m_assets.musics.at(Music::Game).play();
            m_levelWasLoaded = true;
        }
        break;
    case MenuItem::Fps:
        m_menu.SwitchFpsState();
        m_menu.EnterOptions(m_window);
        break;
    case MenuItem::Volume:
        (m_menu.IsVolumeEnabled()) ? DisableVolume() : EnableVolume();
        m_menu.SwitchVolumeState();
        m_menu.EnterOptions(m_window);
        break;
    case MenuItem::Return:
        (m_menu.IsGameOnPause()) ? m_menu.ReinitializeAfterGamePause(m_window) : m_menu.Initialize(m_window);
        break;
    case MenuItem::Resume:
        m_assets.musics.at(Music::Menu).pause();
        m_assets.musics.at(Music::Game).play();
        m_state = AppState::Playing;
        break;
    case MenuItem::Options:
        m_menu.EnterOptions(m_window);
        break;
    case MenuItem::Exit:
        m_window.close();
        break;
    }
}

void CGameController::ProcessGameUserInputEvents(const sf::Event &event)
{
    if (event.type == sf::Event::KeyPressed)
    {
        switch (event.key.code)
        {
        case sf::Keyboard::Escape:
            m_assets.musics.at(Music::Game).pause();
            m_assets.musics.at(Music::Menu).play();
            m_assets.sounds.at(Sound::MenuOpen).Play();
            m_menu.ReinitializeAfterGamePause(m_window);
            SetScreenViewToDefault();
            m_state = AppState::InMenu;
            break;
        case sf::Keyboard::Space:
            m_assets.sounds.at(Sound::RunnerShoot).Play();
            m_runner.ShootBullet(*m_level);
            break;
        default:
            break;
        }
    }

    if (event.type == sf::Event::MouseMoved)
    {
        UpdateCursor(event);
    }
}

void CGameController::InitializeMenu()
{
    SetScreenViewToDefault();
    m_menu.Initialize(m_window);
}

void CGameController::InitializeNewGame()
{
    InitializeGameEntities();
    m_state = AppState::Playing;
}

void CGameController::InitializeGameEntities()
{
    m_runner.Clear();
    m_enemies.clear();
    m_platforms.clear();
    m_bonuses.clear();
    m_textBoxes.clear();

    if (m_levelNo == 1)
    {
        m_runner.Initialize(*m_level);
    }
    else
    {
        m_runner.Initialize(*m_level, m_runner.GetScore(), m_runner.GetHealth());
    }

    const std::vector<TmxObject> &platforms = m_level->GetObjects("moving-platform");

    for (auto &platform : platforms)
    {
        sf::Vector2f movement;

        if (platform.type == "x")
        {
            movement = sf::Vector2f(1, 0);
        }
        else if (platform.type == "y")
        {
            movement = sf::Vector2f(0, 1);
        }

        sf::Vector2f position = sf::Vector2f(platform.rect.left, platform.rect.top);
        sf::Vector2f bounds = sf::Vector2f(platform.rect.width, platform.rect.height);

        m_platforms.push_back(new CPlatform(*m_level, movement, position, bounds));
    }

    const std::vector<TmxObject> &tmxBonuses = m_level->GetObjects("bonus");

    for (const auto &tmxBonus : tmxBonuses)
    {
        sf::Vector2f position = sf::Vector2f(tmxBonus.rect.left, tmxBonus.rect.top);
        sf::Vector2f bounds = sf::Vector2f(tmxBonus.rect.width, tmxBonus.rect.height);

        if (tmxBonus.type == "heart")
        {
            m_bonuses.push_back(new CBonus(Entity::Heart, position, bounds));
        }
        else if (tmxBonus.type == "coin")
        {
            m_bonuses.push_back(new CBonus(Entity::Coin, position, bounds));
        }
    }

    const std::vector<TmxObject> &tmxEnemies = m_level->GetObjects("enemy");

    for (const auto &tmxEnemy : tmxEnemies)
    {
        sf::Vector2f position = sf::Vector2f(tmxEnemy.rect.left, tmxEnemy.rect.top);
        sf::Vector2f bounds = sf::Vector2f(tmxEnemy.rect.width, tmxEnemy.rect.height);

        if (tmxEnemy.type == "mage")
        {
            m_enemies.push_back(new CEnemy(*m_level, Entity::Enemy, position, bounds));
        }
        else if (tmxEnemy.type == "cannon")
        {
            m_enemies.push_back(new CEnemy(*m_level, Entity::Cannon, position, bounds));
        }
    }
}

void CGameController::UpdateEnemies()
{
    for (auto &enemy : m_enemies)
    {
        enemy->Update(m_elapsedTime, m_runner, m_assets);

        if (m_runner.ToFloatRect().intersects(enemy->ToFloatRect()))
        {
            if ((!m_runner.IsAttacked()) && (enemy->IsAbleToHurt()))
            {
                m_assets.sounds.at(Sound::RunnerDamage).Play();
                m_runner.OnContact(*enemy);
            }
        }

        std::vector<CBullet*> &bullets = m_runner.GetBullets();
        for (auto &bullet : bullets)
        {
            if ((bullet->ToFloatRect().intersects(enemy->ToFloatRect())) && (bullet->IsAbleToHurt()))
            {
                bullet->OnContact(*enemy);
                enemy->OnContact(*bullet);
            }
        }

        bullets = enemy->GetBullets();
        for (auto &bullet : bullets)
        {
            bullet->Update(m_elapsedTime, m_tmxObjects);

            if ((bullet->ToFloatRect().intersects(m_runner.ToFloatRect())) && (bullet->IsAbleToHurt()))
            {
                bullet->OnContact(m_runner);
                m_runner.OnContact(*bullet);
            }
        }

        if (!enemy->IsAlive())
        {
            if (rand() % 10 + 1 > 5)
            {
                m_bonuses.push_back(new CBonus(Entity::Heart, enemy->GetPosition(), sf::Vector2f(32, 32)));
            }
        }
    }

    auto isEnemyDead = [](CEnemy *enemy)
    {
        if (!enemy->IsAlive())
        {
            delete enemy;
            return true;
        }

        return false;
    };

    m_enemies.erase(std::remove_if(m_enemies.begin(), m_enemies.end(), isEnemyDead), m_enemies.end());
}

void CGameController::DrawEnemies()
{
    for (const auto &enemy : m_enemies)
    {
        enemy->Draw(m_window);
    }
}

void CGameController::UpdatePlatforms()
{
    for (auto &platform : m_platforms)
    {
        if (m_runner.ToFloatRect().intersects(platform->ToFloatRect()))
        {
            m_runner.OnContact(*platform);
        }

        platform->Update(m_elapsedTime);

        std::vector<CBullet*> &bullets = m_runner.GetBullets();
        for (auto &bullet : bullets)
        {
            if (bullet->ToFloatRect().intersects(platform->ToFloatRect()))
            {
                bullet->OnContact(*platform);
            }
        }
    }
}

void CGameController::DrawPlatforms()
{
    for (const auto &platform : m_platforms)
    {
        platform->Draw(m_window);
    }
}

void CGameController::UpdateBonuses()
{
    for (auto &bonus : m_bonuses)
    {
        bonus->Update(m_elapsedTime);

        bool isBonusNeedToBeGained = !((bonus->GetType() == Entity::Heart) && (m_runner.GetHealth() > 2));

        if ((m_runner.ToFloatRect().intersects(bonus->ToFloatRect())) && (isBonusNeedToBeGained))
        {
            switch (bonus->GetType())
            {
            case Entity::Coin:
                m_assets.sounds.at(Sound::CoinGain).Play();
                break;
            case Entity::Heart:
                m_assets.sounds.at(Sound::HeartGain).Play();
                break;
            default:
                break;
            }

            if (bonus->GetType() == Entity::Coin)
            {
                CTextBox *textBox = new CTextBox("+1", bonus->GetPosition());
                m_textBoxes.push_back(textBox);
            }

            bonus->OnContact(m_runner);
            m_runner.OnContact(*bonus);
        }
    }

    auto isBonusGained = [](CBonus *bonus)
    {
        if (!bonus->IsAlive())
        {
            delete bonus;
            return true;
        }

        return false;
    };

    m_bonuses.erase(std::remove_if(m_bonuses.begin(), m_bonuses.end(), isBonusGained), m_bonuses.end());
}

void CGameController::DrawBonuses()
{
    for (const auto &bonus : m_bonuses)
    {
        bonus->Draw(m_window);
    }
}

void CGameController::UpdateTextBoxes()
{
    for (auto &textBox : m_textBoxes)
    {
        textBox->Update(m_elapsedTime);
    }

    auto isTextBoxShown = [](CTextBox *textBox)
    {
        if (!textBox->IsAlive())
        {
            delete textBox;
            return true;
        }

        return false;
    };

    m_textBoxes.erase(std::remove_if(m_textBoxes.begin(), m_textBoxes.end(), isTextBoxShown), m_textBoxes.end());
}

void CGameController::DrawTextBoxes()
{
    for (const auto &textBox : m_textBoxes)
    {
        textBox->Draw(m_window);
    }
}

void CGameController::MoveScreenViewByRunnerPosition()
{
    const sf::Vector2f screenSize = static_cast<sf::Vector2f>(m_window.getSize());
    sf::Vector2f position = m_runner.GetPosition();

    if (position.x < screenSize.x / 2)
    {
        position.x = screenSize.x / 2;
    }
    else if (position.x > (FIELD_WIDTH * BLOCK_SIZE) - (screenSize.x / 2))
    {
        position.x = (FIELD_WIDTH * BLOCK_SIZE) - (screenSize.x / 2);
    }

    if (position.y < screenSize.y / 2)
    {
        position.y = screenSize.y / 2;
    }
    else if (position.y > (FIELD_HEIGHT * BLOCK_SIZE) - screenSize.y / 2)
    {
        position.y = (FIELD_HEIGHT * BLOCK_SIZE) - screenSize.y / 2;
    }

    m_view.setCenter(position);
    m_window.setView(m_view);
}

void CGameController::SetScreenViewToDefault()
{
    const sf::Vector2f screenSize = static_cast<sf::Vector2f>(m_window.getSize());
    m_view.reset(sf::FloatRect(0.f, 0.f, screenSize.x, screenSize.y));
    m_window.setView(m_view);
}

void CGameController::ProcessWindowResizing(const sf::Event &event)
{
    const float screenWidth = static_cast<float>(event.size.width);
    const float screenHeight = static_cast<float>(event.size.height);
    m_view = sf::View(sf::FloatRect(0.f, 0.f, screenWidth, screenHeight));

    m_assets.images.at(Image::GameBackground).ResizeToTargetSize(m_window);
    m_assets.images.at(Image::MenuBackground).ResizeToTargetSize(m_window);

    if (m_menu.IsGameOnPause())
    {
        m_menu.ReinitializeAfterGamePause(m_window);
    }
    else if (m_menu.IsOnGameLoss())
    {
        m_menu.ReinitializeAfterGameLoss(m_window, m_runner.GetScore());
    }
    else if (m_menu.IsOnGameWin())
    {
        m_menu.InitializeOnGameWin(m_window, m_runner.GetScore());
    }
    else
    {
        m_menu.Initialize(m_window);
    }

    m_window.setView(m_view);
}

void CGameController::OpenWindow()
{
    // TODO: why this doesn't looks good in the game?
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    m_window.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), WINDOW_TITLE, sf::Style::Default);
    m_window.setIcon(128, 128, m_assets.mAppIcon.getPixelsPtr());

    // m_window.setVerticalSyncEnabled(true);
    m_window.setKeyRepeatEnabled(false);
    m_window.setMouseCursorVisible(false);
    m_window.setFramerateLimit(240);
}

void CGameController::EnableVolume()
{
    m_assets.SetVolume(100);
}

void CGameController::DisableVolume()
{
    m_assets.SetVolume(0);
}

float CGameController::GetFps() const
{
    return 1.f / m_elapsedTime;
}

void CGameController::UpdateCursor(const sf::Event &event)
{
    CSpriteFile& cursor = m_assets.images.at(Image::Cursor);

    const float x = static_cast<float>(event.mouseMove.x);
    const float y = static_cast<float>(event.mouseMove.y);
    cursor.SetPosition(sf::Vector2f(x, y));

    const sf::Vector2f screenSize = m_view.getSize();
    sf::FloatRect rect = cursor.ToFloatRect();

    if (rect.left <= 0)
    {
        cursor.SetPosition(sf::Vector2f(0, rect.top));
    }
    else if (rect.left >= screenSize.x)
    {
        cursor.SetPosition(sf::Vector2f(screenSize.x, rect.top));
    }
    else if (rect.top <= 0)
    {
        cursor.SetPosition(sf::Vector2f(rect.left, 0));
    }
    else if (rect.top >= screenSize.y)
    {
        cursor.SetPosition(sf::Vector2f(rect.left, screenSize.y));
    }
}

void CGameController::LevelRawMemoryRealloc(unsigned levelNo)
{
    TmxLevel *temp = m_level;
    delete temp;
    m_level = nullptr;

    m_level = new TmxLevel;
    m_level->LoadFromFile("level" + std::to_string(levelNo) + ".tmx");

    m_tmxObjects.clear();
    m_tmxObjects = m_level->GetAllObjects();
}
