#include "stdafx.h"
#include "SpriteFile.h"
#include "Animation.h"

void CAnimation::Create(const std::string &name, const sf::Texture &texture, int x, int y, int w, int h, int amount, float speed, int step, bool isLoop)
{
    FrameCollection collection;

    collection.speed = speed;
    collection.isLoop = isLoop;
    collection.counter = 0;
    collection.isPlay = true;
    collection.amount = amount;
    collection.sprite.SetTexture(texture);

    for (unsigned i = 0; i < collection.amount; i++)
    {
        collection.normalFrames.push_back(sf::IntRect(x + (i * step), y, w, h));
        collection.flippedFrames.push_back(sf::IntRect(x + (i * step) + w, y, -w, h));
    }

    mCollections.insert(std::make_pair(name, collection));
    mCurrentName = std::move(name);
    m_bounds = sf::Vector2f(static_cast<float>(w), static_cast<float>(h));
}

// TODO: add isLoop parametr
void CAnimation::LoadFromXml(const std::string &fileName, sf::Texture &texture)
{
    TiXmlDocument xmlDocument(fileName.c_str());

    if (!xmlDocument.LoadFile())
    {
        throw std::runtime_error("failed to load animation data from " + fileName);
    }

    TiXmlElement *head = xmlDocument.FirstChildElement("sprites");
    TiXmlElement *animationElement = head->FirstChildElement("animation");

    while (animationElement != nullptr)
    {
        FrameCollection collection;
        mCurrentName = animationElement->Attribute("title");
        int delay = std::atoi(animationElement->Attribute("delay"));
        collection.speed = 1000.0f / delay;
        collection.sprite.SetTexture(texture);

        TiXmlElement *cut = animationElement->FirstChildElement("cut");

        while (cut != nullptr)
        {
            int x = std::atoi(cut->Attribute("x"));
            int y = std::atoi(cut->Attribute("y"));
            int w = std::atoi(cut->Attribute("w"));
            int h = std::atoi(cut->Attribute("h"));

            collection.normalFrames.push_back(sf::IntRect(x, y, w, h));
            collection.flippedFrames.push_back(sf::IntRect(x + w, y, -w, h));

            cut = cut->NextSiblingElement("cut");
        }

        mCollections.insert(std::make_pair(mCurrentName, collection));
        animationElement = animationElement->NextSiblingElement("animation");
    }
}

void CAnimation::Set(const std::string &name)
{
    mCurrentName = std::move(name);
    mCollections.at(mCurrentName).isFlip = false;
}

void CAnimation::Draw(sf::RenderTarget &window) const
{
    mCollections.at(mCurrentName).sprite.Draw(window);
}

void CAnimation::Flip(bool isFlip)
{
    mCollections[mCurrentName].isFlip = isFlip;
}

void CAnimation::Update(float elapsedTime)
{
    FrameCollection &collection = mCollections.at(mCurrentName);

    collection.counter += collection.speed * elapsedTime;

    if (collection.counter > static_cast<unsigned>(collection.amount))
    {
        collection.counter = 0;
        if (!collection.isLoop)
        {
            collection.isPlay = false;
            return;
        }
    }

    unsigned index = static_cast<unsigned>(floor(collection.counter));

    if ((index < 0) || (index > collection.amount - 1))
    {
        std::cerr << "Animation error: out of scopes!" << std::endl;
        return;
    }

    if (collection.isFlip)
    {
        collection.sprite.SetTextureRect(collection.flippedFrames[index]);
    }
    else
    {
        collection.sprite.SetTextureRect(collection.normalFrames[index]);
    }
}

void CAnimation::Play()
{
    mCollections.at(mCurrentName).isPlay = true;
}

void CAnimation::Play(const std::string &name)
{
    mCollections.at(name).isPlay = true;
}

void CAnimation::Pause()
{
    mCollections.at(mCurrentName).isPlay = false;
}

bool CAnimation::IsPlaying() const
{
    return mCollections.at(mCurrentName).isPlay;
}

void CAnimation::SetPosition(const sf::Vector2f &position)
{
    m_position = position;
    mCollections.at(mCurrentName).sprite.SetPosition(position);
}

sf::Vector2f CAnimation::GetOrigin() const
{
    return mCollections.at(mCurrentName).sprite.GetOrigin();
}

sf::Vector2f CAnimation::GetPosition() const
{
    return mCollections.at(mCurrentName).sprite.GetPosition();
}

sf::FloatRect CAnimation::GetGlobalBounds() const
{
    return mCollections.at(mCurrentName).sprite.ToFloatRect();
}

float CAnimation::GetWidth() const
{
    return m_bounds.x;
}

float CAnimation::GetHeight() const
{
    return m_bounds.y;
}

void CAnimation::SetScale(const sf::Vector2f &scale)
{
    for (auto &animation : mCollections)
    {
        animation.second.sprite.SetScale(
            sf::Vector2f(
                scale.x / animation.second.sprite.GetScale().x,
                scale.y / animation.second.sprite.GetScale().y
            )
        );
    }

    m_bounds.x *= scale.x;
    m_bounds.y *= scale.y;
}

void CAnimation::SetColor(const sf::Color &color)
{
    mCollections.at(mCurrentName).sprite.SetColor(color);
}

void CAnimation::SetRotation(float angle)
{
    mCollections.at(mCurrentName).sprite.SetRotation(angle);
}

void CAnimation::SetOrigin(const sf::Vector2f &origin)
{
    mCollections.at(mCurrentName).sprite.SetOrigin(origin);
}
