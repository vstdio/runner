#include "stdafx.h"
#include "constants.h"
#include "TextBox.h"

CTextBox::CTextBox(const std::string &str, const sf::Vector2f &position)
{
    if (!mFont.loadFromFile(FONTS_DIR + "andy.ttf"))
    {
        throw std::runtime_error("failed to load font into textbox");
    }

    mText.setFont(mFont);
    mText.setFillColor(sf::Color::White);
    mText.setPosition(position);
    mText.setString(str);
}

void CTextBox::Update(float elapsedTime)
{
    const float step = TEXTBOX_MOVEMENT_SPEED * elapsedTime;
    mText.move(0, -step);

    mDuration += elapsedTime;
    sf::Color color = mText.getFillColor();
    color.a = sf::Uint8(255 - 100 * mDuration);
    mText.setFillColor(color);

    mIsAlive = (color.a >= sf::Uint8(20));
}

void CTextBox::Draw(sf::RenderTarget &target) const
{
    target.draw(mText);
}

bool CTextBox::IsAlive() const
{
    return mIsAlive;
}
