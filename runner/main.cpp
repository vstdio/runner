#include "stdafx.h"
#include "constants.h"
#include "SpriteFile.h"
#include "ResourceHandler.h"
#include "TextBox.h"
#include "GameMenu.h"
#include "Animation.h"
#include "TmxLevel.h"
#include "ContactHandler.h"
#include "Bonus.h"
#include "Platform.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Runner.h"
#include "GameController.h"

int main()
{
    unsigned seed = static_cast<unsigned>(time(NULL));
    srand(seed);

    try
    {
        CGameController application;
        application.EnterGameLoop();
    }
    catch (const std::exception &ex)
    {
        std::cerr << ex.what() << std::endl;
        return 1;
    }

    return 0;
}
