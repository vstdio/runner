#pragma once

class CTextBox
{
public:
    CTextBox(const std::string &str, const sf::Vector2f &position);
    ~CTextBox() = default;

    void Update(float elapsedTime);

    void Draw(sf::RenderTarget &target) const;
    bool IsAlive() const;

private:
    sf::Font mFont;
    sf::Text mText;

    bool mIsAlive = true;
    float mDuration = 0;
};
