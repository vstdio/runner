#pragma once

class CSpriteFile
{
public:
    CSpriteFile() = default;
    ~CSpriteFile() = default;

    void LoadFromFile(const std::string &fileName);

    void SetPosition(const sf::Vector2f &position);
    void SetScale(const sf::Vector2f &scale);
    void SetTexture(const sf::Texture &texture);
    void SetTextureRect(const sf::IntRect &rect);
    void SetColor(const sf::Color &color);
    void SetOrigin(const sf::Vector2f &origin);
    void SetRotation(float angle);

    void ResizeToTargetSize(sf::RenderWindow &target);

    void Draw(sf::RenderTarget &target) const;

    sf::Vector2f GetPosition() const;
    sf::Vector2f GetOrigin() const;
    sf::Vector2f GetScale() const;
    sf::FloatRect ToFloatRect() const;

private:
    sf::Texture m_texture;
    sf::Sprite m_sprite;
};
