#pragma once

class CBonus : public IContactHandler
{
public:
    CBonus(const Entity type, const sf::Vector2f &position, const sf::Vector2f &bounds);

    bool IsAlive() const;

    void OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object);
    void OnContact(IContactHandler &entity);
    Entity GetType() const;

    void Update(const float elapsedTime);

    void Draw(sf::RenderWindow &window) const;
    sf::FloatRect ToFloatRect() const;

    sf::Vector2f GetMovementVector() const;
    sf::Vector2f GetPosition() const;

private:
    sf::Texture m_texture;
    CAnimation m_animation;

    sf::Vector2f m_position;
    sf::Vector2f m_bounds;

    int m_health = 100;
    Entity m_type;
};
