#pragma once

#include "TmxLevel.h"

class CRunner final : public IContactHandler
{
public:
    enum class State
    {
        Stay,
        Walk,
        Jump,
        Climb,
        None
    };

    CRunner(CGameResources &resources);
    ~CRunner() noexcept;

    void HandleKeyboardEvents();

    void Initialize(TmxLevel &level, unsigned score = 0, unsigned health = 3);
    void Update(const float elapsedTime);
    void Clear();

    void Draw(sf::RenderTarget &target) const;

    void ShootBullet(TmxLevel &level);

    int GetScore() const;
    int GetHealth() const;
    float GetSpeed() const;

    State GetState() const;
    sf::Vector2f GetPosition() const;
    std::vector<CBullet*> GetBullets() const;

    void OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object);
    void OnContact(IContactHandler &entity);

    Entity GetType() const;
    sf::FloatRect ToFloatRect() const;
    sf::Vector2f GetMovementVector() const;

    DirectionX GetDirectionX() const;
    DirectionY GetDirectionY() const;

    bool IsAttacked() const;
    bool IsAlive() const;

    void MoveOnStep(const Axis axis, const float elapsedTime);

private:
    sf::Texture m_texture;
    CAnimation m_animation;
    CSpriteFile m_parachute;

    sf::Vector2f m_position;
    sf::Vector2f m_movement;
    sf::Vector2f m_bounds;

    DirectionX m_directionX;
    DirectionY m_directionY;

    State m_state;

    std::vector<TmxObject> m_objects;
    std::vector<CBullet*> m_bullets;

    sf::Clock m_clock;

    CGameResources &m_res;

    float m_speed;
    float m_elapsedTime;

    int m_score;
    int m_health;

    bool m_isShooting = false;
    bool m_isOnLadder = false;
    bool m_isAlive = true;
    bool m_isOnTopLadderBlock = false;
    bool m_isGoingDownOnLadder = false;
    bool m_isAttacked = false;
    bool m_isOnPlatform = false;
    bool m_isParachuteOpened = false;

    void UpdateAnimation(const float elapsedTime);
    void UpdateBullets(const float elapsedTime);
};
