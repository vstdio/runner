#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <functional>
#include <algorithm>
#include <iterator>

#define _USE_MATH_DEFINES

#include <cmath>
#include <math.h>
