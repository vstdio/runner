#pragma once

enum class MenuItem
{
    Fps,
    Volume,
    Return,
    Start,
    Resume,
    Options,
    Exit
};

class CGameMenu
{
public:
    CGameMenu();
    ~CGameMenu() = default;

    void Initialize(const sf::RenderTarget &target);
    void ReinitializeAfterGamePause(const sf::RenderTarget &target);
    void ReinitializeAfterGameLoss(const sf::RenderTarget &target, int totalScore);
    void InitializeOnGameWin(const sf::RenderTarget &target, int totalScore);
    void EnterOptions(sf::RenderTarget &target);

    void Draw(sf::RenderTarget &target) const;
    void DrawGameInterface(sf::RenderTarget &target, const unsigned score, const unsigned health);
    void DrawFps(sf::RenderTarget &target, const float fps);
    void DrawLoadingScreen(sf::RenderTarget &target);

    void MoveUp();
    void MoveDown();

    void SetCurrentItem(int itemIndex);

    unsigned GetItemsAmount() const;
    sf::IntRect GetItemAsRect(int i) const;
    MenuItem GetPressedItem() const;

    bool IsOnGameWin() const;
    bool IsGameOnPause() const;
    bool IsHovered(int i) const;
    bool IsOnGameLoss() const;
    bool IsInOptions() const;
    bool IsFpsEnabled() const;
    bool IsVolumeEnabled() const;

    void SwitchFpsState();
    void SwitchVolumeState();

private:
    void UpdateItemState(unsigned itemNo, bool selected);

    void DrawHealth(sf::RenderTarget &target, unsigned health);
    void DrawScore(sf::RenderTarget &target, unsigned score);

    int m_selectedItemIndex = 0;

    bool mIsOnGameWin = false;
    bool m_isOnGamePause = false;
    bool m_isGameOnLoss = false;
    bool m_isInOptions = false;
    bool m_isFpsEnabled = false;
    bool m_isVolumeEnabled = true;

    sf::Font m_font;
    sf::Text m_text;

    CSpriteFile m_heart;

    std::vector<sf::Text> m_items;
    std::vector<sf::Text> m_information;
    std::vector<bool> mIsHovered;
};
