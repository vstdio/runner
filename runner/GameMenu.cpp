#include "stdafx.h"
#include "constants.h"
#include "ResourceHandler.h"
#include "GameMenu.h"

static const int initialItemsAmount = 3;
static const int itemsAmountOnGamePause = 4;
static const int itemsAmountOnGameLoss = 2;
static const int itemsAmountInOptions = 3;
static const int ITEMS_AMOUNT_ON_GAME_WIN = 2;

CGameMenu::CGameMenu()
{
    m_font.loadFromFile("resources/fonts/andy.ttf");
    m_text.setFont(m_font);

    m_heart.LoadFromFile(IMAGE_DIR + "heart.png");
    m_heart.SetTextureRect(sf::IntRect(0, 0, 32, 28));
}

void CGameMenu::Initialize(const sf::RenderTarget &target)
{
    m_selectedItemIndex = 0;
    m_items.clear();
    m_information.clear();
    m_isInOptions = false;
    mIsOnGameWin = false;

    sf::Text button;

    for (unsigned i = 0; i < initialItemsAmount; ++i)
    {
        button.setFont(m_font);
        button.setOutlineThickness(2);
        button.setOutlineColor(sf::Color(191, 239, 255));
        button.setString(MENU_ITEMS_INITIAL[i]);

        float x = static_cast<float>((target.getSize().x / 2) - button.getGlobalBounds().width / 2);
        float y = static_cast<float>(target.getSize().y / (initialItemsAmount + 1) * (i + 1));

        button.setPosition(sf::Vector2f(x, y));
        m_items.push_back(button);
        UpdateItemState(i, (i == m_selectedItemIndex));
    }

    mIsHovered.assign(GetItemsAmount(), false);
}

void CGameMenu::ReinitializeAfterGamePause(const sf::RenderTarget &target)
{
    m_selectedItemIndex = 0;
    UpdateItemState(m_selectedItemIndex, true);
    m_isOnGamePause = true;
    m_isInOptions = false;
    mIsOnGameWin = false;
    m_items.clear();
    m_information.clear();

    sf::Text button;

    for (unsigned i = 0; i < itemsAmountOnGamePause; ++i)
    {
        button.setFont(m_font);
        button.setOutlineThickness(2);
        button.setOutlineColor(sf::Color(191, 239, 255));
        button.setString(MENU_ITEMS_ON_GAME_PAUSE[i]);

        float x = static_cast<float>((target.getSize().x / 2) - button.getGlobalBounds().width / 2);
        float y = static_cast<float>(target.getSize().y / (itemsAmountOnGamePause + 1) * (i + 1));

        button.setPosition(sf::Vector2f(x, y));
        m_items.push_back(button);
        UpdateItemState(i, (i == m_selectedItemIndex));
    }

    mIsHovered.assign(GetItemsAmount(), false);
    mIsHovered[m_selectedItemIndex] = true;
}

void CGameMenu::ReinitializeAfterGameLoss(const sf::RenderTarget &target, int totalScore)
{
    m_selectedItemIndex = 0;
    UpdateItemState(m_selectedItemIndex, true);
    m_isOnGamePause = false;
    m_isGameOnLoss = true;
    m_isInOptions = false;
    mIsOnGameWin = false;
    m_items.clear();
    m_information.clear();

    sf::Text button;

    float offsetY = 0.4f * target.getSize().y;

    for (unsigned i = 0; i < itemsAmountOnGameLoss; ++i)
    {
        button.setFont(m_font);
        button.setOutlineThickness(2);
        button.setOutlineColor(sf::Color(191, 239, 255));
        button.setString(MENU_ITEMS_ON_GAME_LOSS[i]);

        float x = static_cast<float>((target.getSize().x / 2) - button.getGlobalBounds().width / 2);
        float y = static_cast<float>((target.getSize().y) / (itemsAmountOnGamePause + 1) * (i + 1)) + offsetY;

        button.setPosition(sf::Vector2f(x, y));
        m_items.push_back(button);
        UpdateItemState(i, (i == m_selectedItemIndex));
    }

    mIsHovered.assign(GetItemsAmount(), false);
    mIsHovered[m_selectedItemIndex] = true;

    // ������������� ��������� � ����������� � ���������
    sf::Text text;

    offsetY = 0.65f * target.getSize().y;

    text.setFont(m_font);
    text.setCharacterSize(70);
    text.setOutlineThickness(2);
    text.setOutlineColor(sf::Color(30, 144, 255));
    text.setFillColor(sf::Color(191, 239, 255));
    text.setString("Game Over!");

    text.setPosition(target.getSize().x / 2.0f - text.getGlobalBounds().width / 2.0f, (target.getSize().y - offsetY) / 2);
    m_information.push_back(text);

    offsetY = 0.35f * target.getSize().y;

    text.setCharacterSize(70);
    text.setOutlineThickness(2);
    text.setOutlineColor(sf::Color(30, 144, 255));
    text.setFillColor(sf::Color(191, 239, 255));
    text.setString("Score: " + std::to_string(totalScore));
    
    text.setPosition(target.getSize().x / 2.0f - text.getGlobalBounds().width / 2.0f, (target.getSize().y - offsetY) / 2);
    m_information.push_back(text);
}

void CGameMenu::InitializeOnGameWin(const sf::RenderTarget &target, int totalScore)
{
    m_selectedItemIndex = 0;
    UpdateItemState(m_selectedItemIndex, true);
    mIsOnGameWin = true;
    m_isOnGamePause = false;
    m_isGameOnLoss = false;
    m_isInOptions = false;
    m_items.clear();
    m_information.clear();

    float offsetY = 0.4f * target.getSize().y;

    // ������������� ��������� � ����������� � ��������
    sf::Text text;

    offsetY = 0.65f * target.getSize().y;

    text.setFont(m_font);
    text.setCharacterSize(70);
    text.setOutlineThickness(2);
    text.setOutlineColor(sf::Color(30, 144, 255));
    text.setFillColor(sf::Color(191, 239, 255));
    text.setString("Congratulations! You win.");

    text.setPosition(target.getSize().x / 2.0f - text.getGlobalBounds().width / 2.0f, (target.getSize().y - offsetY) / 2);
    m_information.push_back(text);

    offsetY = 0.35f * target.getSize().y;

    text.setCharacterSize(70);
    text.setOutlineThickness(2);
    text.setOutlineColor(sf::Color(30, 144, 255));
    text.setFillColor(sf::Color(191, 239, 255));
    text.setString("Score: " + std::to_string(totalScore));

    text.setPosition(target.getSize().x / 2.0f - text.getGlobalBounds().width / 2.0f, (target.getSize().y - offsetY) / 2);
    m_information.push_back(text);

    sf::Text button;

    for (unsigned i = 0; i < ITEMS_AMOUNT_ON_GAME_WIN; ++i)
    {
        button.setFont(m_font);
        button.setOutlineThickness(2);
        button.setOutlineColor(sf::Color(191, 239, 255));
        button.setString(MENU_ITEMS_ON_GAME_WIN[i]);

        float x = static_cast<float>((target.getSize().x / 2) - button.getGlobalBounds().width / 2);
        float y = static_cast<float>((target.getSize().y) / (itemsAmountOnGamePause + 1) * (i + 1)) + offsetY;

        button.setPosition(sf::Vector2f(x, y));
        m_items.push_back(button);
        UpdateItemState(i, (i == m_selectedItemIndex));
    }
}

void CGameMenu::EnterOptions(sf::RenderTarget &target)
{
    UpdateItemState(m_selectedItemIndex, true);
    m_isInOptions = true;
    m_isGameOnLoss = false;
    m_items.clear();
    m_information.clear();

    sf::Text button;

    for (unsigned i = 0; i < itemsAmountInOptions; ++i)
    {
        button.setFont(m_font);
        button.setOutlineThickness(2);
        button.setOutlineColor(sf::Color(191, 239, 255));

        if (i == 0)
        {
            button.setString((m_isFpsEnabled) ? ("FPS: ON") : ("FPS: OFF"));
        }
        else if (i == 1)
        {
            button.setString((m_isVolumeEnabled) ? ("Volume: ON") : ("Volume: OFF"));
        }
        else
        {
            button.setString("Return");
        }

        float x = static_cast<float>((target.getSize().x / 2) - button.getGlobalBounds().width / 2);
        float y = static_cast<float>(target.getSize().y / (itemsAmountOnGamePause + 1) * (i + 1));

        button.setPosition(sf::Vector2f(x, y));
        m_items.push_back(button);
        UpdateItemState(i, (i == m_selectedItemIndex));
    }

    mIsHovered.assign(GetItemsAmount(), false);
    mIsHovered[m_selectedItemIndex] = true;
}

void CGameMenu::Draw(sf::RenderTarget &target) const
{
    for (const auto &item : m_items)
    {
        target.draw(item);
    }

    for (const auto &info : m_information)
    {
        target.draw(info);
    }
}

void CGameMenu::DrawGameInterface(sf::RenderTarget &target, const unsigned score, const unsigned health)
{
    DrawHealth(target, health);
    DrawScore(target, score);
}

void CGameMenu::DrawHealth(sf::RenderTarget &target, unsigned health)
{
    m_text.setString("Health: ");

    float offsetX = 30;
    float offsetY = 20;

    const sf::View view = target.getView();

    float x = (view.getCenter().x - view.getSize().x / 2) + offsetX;
    float y = (view.getCenter().y - view.getSize().y / 2) + offsetY;

    m_text.setPosition(sf::Vector2f(x, y));
    target.draw(m_text);

    for (unsigned i = 0; i < health; ++i)
    {
        float interval = 10;

        offsetX = 130;
        offsetY = 30;

        x = (view.getCenter().x - view.getSize().x / 2) + ((m_heart.ToFloatRect().width + interval) * i) + offsetX;
        y = (view.getCenter().y - view.getSize().y / 2) + offsetY;

        m_heart.SetPosition(sf::Vector2f(x, y));
        m_heart.Draw(target);
    }
}

void CGameMenu::DrawScore(sf::RenderTarget &target, unsigned score)
{
    m_text.setString("Score: " + std::to_string(score));

    const float offsetX = 30;
    const float offsetY = 70;

    const sf::View view = target.getView();

    const float x = (view.getCenter().x - view.getSize().x / 2) + offsetX;
    const float y = (view.getCenter().y - view.getSize().y / 2) + offsetY;

    m_text.setPosition(sf::Vector2f(x, y));
    target.draw(m_text);
}

void CGameMenu::DrawFps(sf::RenderTarget &target, const float fps)
{
    std::stringstream strm;
    strm << "FPS: " << std::fixed << std::setprecision(3) << fps;
    m_text.setString(strm.str());

    sf::View currentView = target.getView();

    const sf::Vector2f center = currentView.getCenter();
    const sf::Vector2f size = currentView.getSize();

    const float offsetX = 170.f;
    const float offsetY = 25.f;

    const float x = (center.x + size.x / 2.f) - offsetX;
    const float y = (center.y - size.y / 2.f) + offsetY;

    m_text.setPosition(sf::Vector2f(x, y));
    target.draw(m_text);
}

void CGameMenu::DrawLoadingScreen(sf::RenderTarget &target)
{
    sf::Text text;
    text.setFont(m_font);
    text.setCharacterSize(38);

    sf::RectangleShape rectangle;
    rectangle.setSize(static_cast<sf::Vector2f>(target.getSize()));
    rectangle.setPosition(sf::Vector2f(0, 0));
    rectangle.setFillColor(sf::Color(0, 0, 0, 170));

    const float offsetX = 60.f;
    const float offsetY = 100.f;

    text.setString("Loading...");
    text.setPosition(sf::Vector2f(target.getSize().x - text.getGlobalBounds().width - offsetX, target.getSize().y - offsetY));
    target.draw(rectangle);
    target.draw(text);
}

void CGameMenu::MoveUp()
{
    if (m_selectedItemIndex - 1 >= 0)
    {
        UpdateItemState(m_selectedItemIndex, false);
        m_selectedItemIndex = m_selectedItemIndex - 1;
        UpdateItemState(m_selectedItemIndex, true);

        mIsHovered.assign(GetItemsAmount(), false);
        mIsHovered[m_selectedItemIndex] = true;
    }
}

void CGameMenu::MoveDown()
{
    const unsigned nextNo = static_cast<unsigned>(m_selectedItemIndex + 1);

    if (nextNo < m_items.size())
    {
        m_items[m_selectedItemIndex].setCharacterSize(42);
        m_items[m_selectedItemIndex].setFillColor(sf::Color(128, 128, 128));

        m_selectedItemIndex = nextNo;

        m_items[m_selectedItemIndex].setCharacterSize(44);
        m_items[m_selectedItemIndex].setFillColor(sf::Color(30, 144, 255));

        mIsHovered.assign(GetItemsAmount(), false);
        mIsHovered[m_selectedItemIndex] = true;
    }
}

MenuItem CGameMenu::GetPressedItem() const
{
    MenuItem item;

    switch (m_selectedItemIndex)
    {
    case 0:
        item = (m_isOnGamePause) ? ((m_isInOptions) ? MenuItem::Fps : MenuItem::Resume) : ((m_isInOptions) ? MenuItem::Fps : (MenuItem::Start));
        break;
    case 1:
        item = (m_isOnGamePause) ? ((m_isInOptions) ? MenuItem::Volume : MenuItem::Start)  : (m_isGameOnLoss || mIsOnGameWin) ? MenuItem::Exit : (m_isInOptions) ? MenuItem::Volume : MenuItem::Options;
        break;
    case 2:
        item = (m_isOnGamePause) ? ((m_isInOptions) ? MenuItem::Return : MenuItem::Options) : (m_isInOptions) ? MenuItem::Return : MenuItem::Exit;
        break;
    case 3:
        item = MenuItem::Exit;
        break;
    default:
        break;
    }

    return item;
}

sf::IntRect CGameMenu::GetItemAsRect(int i) const
{
    return sf::IntRect(m_items[i].getGlobalBounds());
}

bool CGameMenu::IsGameOnPause() const
{
    return m_isOnGamePause;
}

void CGameMenu::SetCurrentItem(int itemIndex)
{
    m_items[m_selectedItemIndex].setCharacterSize(42);
    m_items[m_selectedItemIndex].setFillColor(sf::Color(128, 128, 128));

    m_selectedItemIndex = itemIndex;

    m_items[m_selectedItemIndex].setCharacterSize(44);
    m_items[m_selectedItemIndex].setFillColor(sf::Color(30, 144, 255));

    mIsHovered.assign(GetItemsAmount(), false);
    mIsHovered[m_selectedItemIndex] = true;
}

void CGameMenu::UpdateItemState(unsigned itemNo, bool selected)
{
    if ((itemNo >= 0) && (itemNo <= m_items.size()))
    {
        if (selected)
        {
            m_items[itemNo].setCharacterSize(44);
            m_items[itemNo].setFillColor(sf::Color(30, 144, 255));
        }
        else
        {
            m_items[itemNo].setCharacterSize(42);
            m_items[itemNo].setFillColor(sf::Color(128, 128, 128));
        }
    }
}

void CGameMenu::SwitchFpsState()
{
    m_isFpsEnabled = !m_isFpsEnabled;
}

void CGameMenu::SwitchVolumeState()
{
    m_isVolumeEnabled = !m_isVolumeEnabled;
}

unsigned CGameMenu::GetItemsAmount() const
{
    return m_items.size();
}

bool CGameMenu::IsHovered(int i) const
{
    return mIsHovered[i];
}

bool CGameMenu::IsOnGameLoss() const
{
    return m_isGameOnLoss;
}

bool CGameMenu::IsOnGameWin() const
{
    return mIsOnGameWin;
}

bool CGameMenu::IsInOptions() const
{
    return m_isInOptions;
}

bool CGameMenu::IsFpsEnabled() const
{
    return m_isFpsEnabled;
}

bool CGameMenu::IsVolumeEnabled() const
{
    return m_isVolumeEnabled;
}
