#include "stdafx.h"
#include "constants.h"
#include "ContactHandler.h"
#include "Bullet.h"

CBullet::CBullet(Entity type, const sf::Vector2f &position, const sf::Vector2f &movement)
    : m_position(position)
    , m_movement(movement)
{
    m_texture.loadFromFile(IMAGE_DIR + "bullet.png");

    switch (type)
    {
    case Entity::Runner:
        m_animation.Create("explode", m_texture, 27, 7, 18, 18, 3, 10, 29, false);
        break;
    case Entity::Cannon:
        m_animation.Create("explode", m_texture, 0, 25, 25, 30, 3, 6, 42, false);
        break;
    default:
        break;
    }

    m_animation.Create("move", m_texture, 7, 10, 8, 8, 1, 0, 8, true);
}

void CBullet::Update(const float elapsedTime, const std::vector<TmxObject> &objects)
{
    m_animation.Set("move");

    const float step = m_speed * elapsedTime;

    m_position.x += m_movement.x * step;
    m_position.y += m_movement.y * step;

    for (unsigned i = 0; i < objects.size(); ++i)
    {
        if (ToFloatRect().intersects(objects[i].rect))
        {
            if ((objects[i].name == "solid") || (objects[i].name == "bullet-solid"))
            {
                m_health = 0;
            }
        }
    }

    if (((m_position.x < 0) || (m_position.x > BLOCK_SIZE * FIELD_WIDTH)) ||
        ((m_position.y < 0) || (m_position.y > BLOCK_SIZE * FIELD_HEIGHT)))
    {
        m_health = 0;
    }

    if (m_health == 0)
    {
        m_animation.Set("explode");

        if (!m_animation.IsPlaying())
        {
            m_isAlive = false;
        }

        m_movement = { 0, 0 };
    }

    m_animation.Update(elapsedTime);
    m_animation.SetPosition(m_position);
}

void CBullet::Draw(sf::RenderTarget &target)
{
    m_animation.Draw(target);
}

sf::FloatRect CBullet::ToFloatRect() const
{
    float width = static_cast<float>(m_animation.GetWidth());
    float height = static_cast<float>(m_animation.GetHeight());

    return sf::FloatRect(m_position, sf::Vector2f(width, height));
}

bool CBullet::IsAlive() const
{
    return m_isAlive;
}

bool CBullet::IsAbleToHurt() const
{
    return (m_health > 0);
}

void CBullet::OnContact(IContactHandler &entity)
{
    if (entity.GetType() == Entity::Platform)
    {
        m_health = 0;
    }

    if (entity.GetType() == Entity::Enemy)
    {
        m_health = 0;
    }

    if (entity.GetType() == Entity::Cannon)
    {
        m_health = 0;
    }

    if (entity.GetType() == Entity::Runner)
    {
        m_health = 0;
    }
}

void CBullet::OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object)
{
    if (category == StaticCategory::BulletSolid)
    {
        m_health = 0;
    }
}

Entity CBullet::GetType() const
{
    return Entity::Bullet;
}

DirectionX CBullet::GetDirectionX() const
{
    return (m_movement.x >= 0) ? DirectionX::Right : DirectionX::Left;
}

DirectionY CBullet::GetDirectionY() const
{
    return DirectionY::None;
}

sf::Vector2f CBullet::GetMovementVector() const
{
    return m_movement;
}

sf::Vector2f CBullet::GetPosition() const
{
    return m_position;
}
