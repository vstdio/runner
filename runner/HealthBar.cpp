#include "stdafx.h"
#include "HealthBar.h"

CHealthBar::CHealthBar(const sf::Vector2f &size, int health)
{
    m_health = health;
    SetSize(size);

    m_backRect.setFillColor(sf::Color::Black);
    m_frontRect.setFillColor(sf::Color::Red);
}

void CHealthBar::SetSize(const sf::Vector2f &size)
{
    m_backRect.setSize(size);
    m_frontRect.setSize(size);
    m_initialFrontRectWidth = size.x;
}

void CHealthBar::SetPosition(const sf::Vector2f &position)
{
    m_backRect.setPosition(position);
    m_frontRect.setPosition(position);
}

void CHealthBar::DecreaseHealth()
{
    sf::Vector2f prevHealthSize = m_frontRect.getSize();

    float healthPortion = m_initialFrontRectWidth / m_health;
    float newHealthWidth = prevHealthSize.x - healthPortion;

    newHealthWidth = (newHealthWidth > 0) ? newHealthWidth : 0;

    m_frontRect.setSize(sf::Vector2f(newHealthWidth, prevHealthSize.y));
}

void CHealthBar::Draw(sf::RenderTarget &target) const
{
    target.draw(m_backRect);
    target.draw(m_frontRect);
}

sf::Vector2f CHealthBar::GetSize() const
{
    return m_backRect.getSize();
}
