#pragma once

#include "sdk/TinyXML/tinyxml.h"
#include "SpriteFile.h"

class CAnimation
{
    struct FrameCollection
    {
        CSpriteFile sprite;

        std::vector<sf::IntRect> flippedFrames;
        std::vector<sf::IntRect> normalFrames;

        float counter;
        float speed;
        unsigned amount;

        bool isPlay;
        bool isFlip;
        bool isLoop;
    };

public:
    CAnimation() = default;
    ~CAnimation() = default;

    // Adds new frame collection into animation container
    void Create(const std::string &name, const sf::Texture &texture, int x, int y, int w, int h, int amount, float speed, int step, bool isLoop);
    void LoadFromXml(const std::string &fileName, sf::Texture &texture);

    void Set(const std::string &name);
    void Update(float elapsedTime);
    void Flip(bool isFlip);

    void Play(const std::string &name);
    void Play();
    void Pause();

    void SetPosition(const sf::Vector2f &position);
    void SetScale(const sf::Vector2f &scale);
    void SetRotation(const float angle);
    void SetOrigin(const sf::Vector2f &origin);
    void SetColor(const sf::Color &color);

    void Draw(sf::RenderTarget &target) const;

    bool IsPlaying() const;

    float GetWidth() const;
    float GetHeight() const;

    sf::Vector2f GetOrigin() const;
    sf::Vector2f GetPosition() const;
    sf::FloatRect GetGlobalBounds() const;

private:
    std::map<std::string, FrameCollection> mCollections;

    std::string mCurrentName;

    sf::Vector2f m_bounds;
    sf::Vector2f m_position;
};
