#include "stdafx.h"
#include "TmxLevel.h"

int TmxObject::GetPropertyInt(const std::string &name)
{
    return atoi(properties.at(name).c_str());
}

float TmxObject::GetPropertyFloat(const std::string &name)
{
    return strtof(properties.at(name).c_str(), NULL);
}

std::string TmxObject::GetPropertyString(const std::string &name)
{
    return properties.at(name);
}

bool TmxLevel::LoadFromFile(const std::string &filename)
{
    TiXmlDocument levelFile(filename.c_str());

    if (!levelFile.LoadFile())
    {
        std::cout << "Loading level \"" << filename << "\" failed." << std::endl;
        return false;
    }

    // �������� � ����������� map
    TiXmlElement *map;
    map = levelFile.FirstChildElement("map");

    // ������ �����: <map version="1.0" orientation="orthogonal"
    // width="10" height="10" tilewidth="34" tileheight="34">
    width = atoi(map->Attribute("width"));//��������� �� ����� ����� �� ��������
    height = atoi(map->Attribute("height"));//�� ��������, ������� �������� ��� ������ � 
    tileWidth = atoi(map->Attribute("tilewidth"));//������� ���������
    tileHeight = atoi(map->Attribute("tileheight"));

    // ����� �������� �������� � ������������� ������� �����
    TiXmlElement *tilesetElement;
    tilesetElement = map->FirstChildElement("tileset");
    firstTileID = atoi(tilesetElement->Attribute("firstgid"));

    // source - ���� �� �������� � ���������� image
    TiXmlElement *image;
    image = tilesetElement->FirstChildElement("image");
    std::string imagepath = image->Attribute("source");

    // �������� ��������� �������
    sf::Image img;

    if (!img.loadFromFile(imagepath))
    {
        std::cout << "Failed to load tile sheet." << std::endl;//���� �� ������� ��������� �������-������� ������ � �������
        return false;
    }


    img.createMaskFromColor(sf::Color(255, 255, 255));//��� ����� �����.������ ��� �����
    tilesetImage.loadFromImage(img);
    tilesetImage.setSmooth(false);//�����������

                                  // �������� ���������� �������� � ����� ��������
    int columns = tilesetImage.getSize().x / tileWidth;
    int rows = tilesetImage.getSize().y / tileHeight;

    // ������ �� ��������������� ����������� (TextureRect)
    std::vector<sf::Rect<int>> subRects;

    for (int y = 0; y < rows; y++)
        for (int x = 0; x < columns; x++)
        {
            sf::Rect<int> rect;

            rect.top = y * tileHeight;
            rect.height = tileHeight;
            rect.left = x * tileWidth;
            rect.width = tileWidth;

            subRects.push_back(rect);
        }

    // ������ �� ������
    TiXmlElement *layerElement;
    layerElement = map->FirstChildElement("layer");

    while (layerElement)
    {
        Layer layer;

        // ���� ������������ opacity, �� ������ ������������ ����, ����� �� ��������� �����������
        if (layerElement->Attribute("opacity") != NULL)
        {
            float opacity = strtof(layerElement->Attribute("opacity"), NULL);
            layer.opacity = static_cast<int>(255 * opacity);
        }
        else
        {
            layer.opacity = 255;
        }

        //  ��������� <data> 
        TiXmlElement *layerDataElement;
        layerDataElement = layerElement->FirstChildElement("data");

        if (layerDataElement == NULL)
        {
            std::cout << "Bad map. No layer information found." << std::endl;
        }

        //  ��������� <tile> - �������� ������ ������� ����
        TiXmlElement *tileElement;
        tileElement = layerDataElement->FirstChildElement("tile");

        if (tileElement == NULL)
        {
            std::cout << "Bad map. No tile information found." << std::endl;
            return false;
        }

        int x = 0;
        int y = 0;

        while (tileElement)
        {
            int tileGID = atoi(tileElement->Attribute("gid"));
            int subRectToUse = tileGID - firstTileID;

            // ������������� TextureRect ������� �����
            if (subRectToUse >= 0)
            {
                sf::Sprite sprite;
                sprite.setTexture(tilesetImage);
                sprite.setTextureRect(subRects[subRectToUse]);

                float xPos = static_cast<float>(x * tileWidth);
                float yPos = static_cast<float>(y * tileHeight);

                sprite.setPosition(sf::Vector2f(xPos, yPos));
                sprite.setColor(sf::Color(255, 255, 255, layer.opacity));

                layer.tiles.push_back(sprite);//���������� � ���� ������� ������
            }

            tileElement = tileElement->NextSiblingElement("tile");

            x++;
            if (x >= width)
            {
                x = 0;
                y++;
                if (y >= height)
                    y = 0;
            }
        }

        layers.push_back(layer);

        layerElement = layerElement->NextSiblingElement("layer");
    }

    // ������ � ���������
    TiXmlElement *objectGroupElement;

    // ���� ���� ���� ��������
    if (map->FirstChildElement("objectgroup") != NULL)
    {
        objectGroupElement = map->FirstChildElement("objectgroup");
        while (objectGroupElement)
        {
            //  ��������� <object>
            TiXmlElement *objectElement;
            objectElement = objectGroupElement->FirstChildElement("object");

            while (objectElement)
            {
                // �������� ��� ������ - ���, ���, �������, � ��
                std::string objectType;
                if (objectElement->Attribute("type") != NULL)
                {
                    objectType = objectElement->Attribute("type");
                }
                std::string objectName;
                if (objectElement->Attribute("name") != NULL)
                {
                    objectName = objectElement->Attribute("name");
                }
                int x = atoi(objectElement->Attribute("x"));
                int y = atoi(objectElement->Attribute("y"));

                int width, height;

                sf::Sprite sprite;
                sprite.setTexture(tilesetImage);
                sprite.setTextureRect(sf::Rect<int>(0, 0, 0, 0));

                const float xPos = static_cast<float>(x);
                const float yPos = static_cast<float>(y);

                sprite.setPosition(sf::Vector2f(xPos, yPos));

                if (objectElement->Attribute("width") != NULL)
                {
                    width = atoi(objectElement->Attribute("width"));
                    height = atoi(objectElement->Attribute("height"));
                }
                else
                {
                    width = subRects[atoi(objectElement->Attribute("gid")) - firstTileID].width;
                    height = subRects[atoi(objectElement->Attribute("gid")) - firstTileID].height;
                    sprite.setTextureRect(subRects[atoi(objectElement->Attribute("gid")) - firstTileID]);
                }

                // ��������� �������
                TmxObject object;
                object.name = objectName;
                object.type = objectType;
                object.sprite = sprite;

                sf::Rect<float> objectRect;

                const float fRectWidth = static_cast<float>(width);
                const float fRectHeight = static_cast<float>(height);

                objectRect.left = xPos;
                objectRect.top = yPos;
                objectRect.width = fRectWidth;
                objectRect.height = fRectHeight;
                object.rect = objectRect;

                // "����������" �������
                TiXmlElement *properties;
                properties = objectElement->FirstChildElement("properties");
                if (properties != NULL)
                {
                    TiXmlElement *prop;
                    prop = properties->FirstChildElement("property");
                    if (prop != NULL)
                    {
                        while (prop)
                        {
                            std::string propertyName = prop->Attribute("name");
                            std::string propertyValue = prop->Attribute("value");

                            object.properties[propertyName] = propertyValue;

                            prop = prop->NextSiblingElement("property");
                        }
                    }
                }


                objects.push_back(object);

                objectElement = objectElement->NextSiblingElement("object");
            }
            objectGroupElement = objectGroupElement->NextSiblingElement("objectgroup");
        }
    }
    else
    {
        std::cout << "No object layers found..." << std::endl;
    }

    return true;
}

TmxObject TmxLevel::GetObject(const std::string &name)
{
    // only the first object with given name
    for (std::size_t i = 0; i < objects.size(); i++)
    {
        if (objects[i].name == name)
        {
            return objects[i];
        }
    }

    throw std::domain_error("TmxObject with name " + name + " not found");
}

std::vector<TmxObject> TmxLevel::GetObjects(const std::string &name)
{
    std::vector<TmxObject> vect;

    for (std::size_t i = 0; i < objects.size(); ++i)
    {
        if (objects[i].name == name)
        {
            vect.push_back(objects[i]);
        }
    }

    return vect;
}

std::vector<TmxObject> TmxLevel::GetAllObjects()
{
    return objects;
};

sf::Vector2i TmxLevel::GetTileSize()
{
    return sf::Vector2i(tileWidth, tileHeight);
}

void TmxLevel::Draw(sf::RenderTarget &target) const
{
    const sf::FloatRect viewportRect = target.getView().getViewport();

    for (const auto &layer : layers)
    {
        for (const auto &tile : layer.tiles)
        {
            if (viewportRect.intersects(tile.getLocalBounds()))
            {
                target.draw(tile);
            }
        }
    }
}
