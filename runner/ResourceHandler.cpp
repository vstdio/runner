#include "stdafx.h"
#include "constants.h"
#include "SpriteFile.h"
#include "SoundFile.h"
#include "ResourceHandler.h"

CGameResources::CGameResources()
{
    LoadSpriteFiles();
    LoadMusicFiles();
    LoadSoundFiles();
}

void CGameResources::LoadSpriteFiles()
{
    m_backgroundMenu.LoadFromFile(IMAGE_DIR + "background-menu.png");
    m_backgroundGame.LoadFromFile(IMAGE_DIR + "background-game.png");
    m_customCursor.LoadFromFile(IMAGE_DIR + "custom-cursor.png");

    if (!mAppIcon.loadFromFile(IMAGE_DIR + "Tree.png"))
    {
        throw std::runtime_error("failed to load Tree.ico");
    }
}

void CGameResources::LoadMusicFiles()
{
    m_musicGame.openFromFile(MUSIC_DIR + "game.ogg");
    m_musicMenu.openFromFile(MUSIC_DIR + "menu.ogg");

    m_musicGame.setLoop(true);
    m_musicMenu.setLoop(true);
}

void CGameResources::LoadSoundFiles()
{
    m_soundRunnerShoot.LoadFromFile(SOUND_DIR + "runner-shoot.ogg");
    m_soundMenuClose.LoadFromFile(SOUND_DIR + "menu-close.wav");
    m_soundMenuOpen.LoadFromFile(SOUND_DIR + "menu-open.wav");
    m_soundMouseClick.LoadFromFile(SOUND_DIR + "mouse-click.wav");
    m_soundMouseOver.LoadFromFile(SOUND_DIR + "mouse-over.wav");
    m_soundCoinGain.LoadFromFile(SOUND_DIR + "coin-gain.wav");
    m_soundHeartGain.LoadFromFile(SOUND_DIR + "heart-gain.wav");
    m_soundRunnerDamage.LoadFromFile(SOUND_DIR + "runner-damage.wav");
    m_soundRunnerDead.LoadFromFile(SOUND_DIR + "runner-dead.wav");
    m_soundCannonShoot.LoadFromFile(SOUND_DIR + "cannon-shoot.wav");
}

void CGameResources::SetVolume(float volume)
{
    SetMusicVolume(volume);
    SetSoundVolume(volume);
}

void CGameResources::SetMusicVolume(float volume)
{
    m_musicGame.setVolume(volume);
    m_musicMenu.setVolume(volume);
}

void CGameResources::SetSoundVolume(float volume)
{
    m_soundMenuClose.SetVolume(volume);
    m_soundMenuOpen.SetVolume(volume);
    m_soundRunnerShoot.SetVolume(volume);
    m_soundMouseClick.SetVolume(volume);
    m_soundMouseOver.SetVolume(volume);
    m_soundCoinGain.SetVolume(volume);
    m_soundHeartGain.SetVolume(volume);
    m_soundRunnerDamage.SetVolume(volume);
    m_soundRunnerDead.SetVolume(volume);
    m_soundCannonShoot.SetVolume(volume);
}
