#pragma once

class CHealthBar
{
public:
    CHealthBar(const sf::Vector2f &size, int health = 3);
    ~CHealthBar() = default;

    void SetSize(const sf::Vector2f &size);
    void SetPosition(const sf::Vector2f &position);

    void DecreaseHealth();

    void Draw(sf::RenderTarget &target) const;
    sf::Vector2f GetSize() const;

private:
    sf::RectangleShape m_backRect;
    sf::RectangleShape m_frontRect;

    float m_initialFrontRectWidth;
    int m_health;
};
