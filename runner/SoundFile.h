#pragma once

class CSoundFile
{
public:
    CSoundFile() = default;
    ~CSoundFile() = default;

    void LoadFromFile(const std::string &fileName);

    void SetVolume(float volume);
    void Play();

private:
    sf::Sound m_sound;
    sf::SoundBuffer m_buffer;
};
