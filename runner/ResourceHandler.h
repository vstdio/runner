#pragma once

#include "SpriteFile.h"
#include "SoundFile.h"

enum class Image
{
    MenuBackground,
    GameBackground,
    Cursor
};

enum class Music
{
    Menu,
    Game
};

enum class Sound
{
    RunnerShoot,
    MenuOpen,
    MenuClose,
    MouseOver,
    MouseClick,
    CoinGain,
    HeartGain,
    RunnerDamage,
    RunnerDead,
    CannonShoot
};

class CGameResources
{
public:
    CGameResources();
    ~CGameResources() = default;

    const std::map<Image, CSpriteFile&> images
    {
        { Image::MenuBackground, m_backgroundMenu },
        { Image::GameBackground, m_backgroundGame },
        { Image::Cursor, m_customCursor }
    };

    const std::map<Music, sf::Music&> musics
    {
        { Music::Game, m_musicGame },
        { Music::Menu, m_musicMenu }
    };

    const std::map<Sound, CSoundFile&> sounds
    {
        { Sound::RunnerShoot, m_soundRunnerShoot },
        { Sound::MenuClose, m_soundMenuClose },
        { Sound::MenuOpen, m_soundMenuOpen },
        { Sound::MouseClick, m_soundMouseClick },
        { Sound::MouseOver, m_soundMouseOver },
        { Sound::CoinGain, m_soundCoinGain },
        { Sound::HeartGain, m_soundHeartGain },
        { Sound::RunnerDamage, m_soundRunnerDamage },
        { Sound::RunnerDead, m_soundRunnerDead },
        { Sound::CannonShoot, m_soundCannonShoot }
    };

    void SetVolume(float volume);

    void SetMusicVolume(float volume);
    void SetSoundVolume(float volume);

    sf::Image mAppIcon;

private:
    void LoadSpriteFiles();
    void LoadMusicFiles();
    void LoadSoundFiles();

    CSpriteFile m_backgroundMenu;
    CSpriteFile m_backgroundGame;
    CSpriteFile m_customCursor;

    sf::Music m_musicGame;
    sf::Music m_musicMenu;

    CSoundFile m_soundRunnerShoot;
    CSoundFile m_soundMenuClose;
    CSoundFile m_soundMenuOpen;
    CSoundFile m_soundMouseClick;
    CSoundFile m_soundMouseOver;
    CSoundFile m_soundCoinGain;
    CSoundFile m_soundHeartGain;
    CSoundFile m_soundRunnerDead;
    CSoundFile m_soundRunnerDamage;
    CSoundFile m_soundCannonShoot;
};
