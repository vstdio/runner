#include "stdafx.h"
#include "SoundFile.h"

void CSoundFile::LoadFromFile(const std::string &fileName)
{
    if (!m_buffer.loadFromFile(fileName))
    {
        throw std::runtime_error("failed to load sound from " + fileName);
    }

    m_sound.setBuffer(m_buffer);
}

void CSoundFile::SetVolume(float volume)
{
    m_sound.setVolume(volume);
}

void CSoundFile::Play()
{
    m_sound.play();
}
