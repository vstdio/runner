#pragma once

enum class DirectionX
{
    Right,
    Left,
    None
};

enum class DirectionY
{
    Up,
    Down,
    None
};

enum class Entity
{
    Runner,
    Enemy,
    Platform,
    Bullet,
    Coin,
    Heart,
    Cannon,
    None
};

enum class Axis
{
    X,
    Y,
};

enum StaticCategory
{
    Solid,
    SolidFromTop,
    SlopeLeft,
    SlopeRight,
    BulletSolid,
    Ladder,
    TopLadderBlock,
    Spike,
    NextLevel,
    EndGame
};

const std::map<std::string, StaticCategory> STATIC_CATEGORY_MAPPING
{
    {"solid",            StaticCategory::Solid},
    {"solid-from-top",   StaticCategory::SolidFromTop},
    {"slope-left",       StaticCategory::SlopeLeft},
    {"slope-right",      StaticCategory::SlopeRight},
    {"bullet-solid",     StaticCategory::BulletSolid},
    {"ladder",           StaticCategory::Ladder},
    {"top-ladder-block", StaticCategory::TopLadderBlock},
    {"spike",            StaticCategory::Spike},
    {"next-level",       StaticCategory::NextLevel},
    {"end",              StaticCategory::EndGame}
};

class IContactHandler
{
public:
    IContactHandler();
    virtual ~IContactHandler();

    virtual void OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object) = 0;
    virtual void OnContact(IContactHandler &entity) = 0;

    virtual Entity GetType() const = 0;

    virtual sf::FloatRect ToFloatRect() const = 0;
    virtual sf::Vector2f GetMovementVector() const = 0;
    virtual sf::Vector2f GetPosition() const = 0;
};
