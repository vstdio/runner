#include "stdafx.h"
#include "constants.h"
#include "Animation.h"
#include "ContactHandler.h"
#include "Bonus.h"

CBonus::CBonus(const Entity type, const sf::Vector2f &position, const sf::Vector2f &bounds)
{
    if (type == Entity::Coin)
    {
        m_texture.loadFromFile(IMAGE_DIR + "coins.png");
        m_animation.Create("rotation", m_texture, 0, 0, 32, 32, 14, 8, 32, true);
    }
    else if (type == Entity::Heart)
    {
        m_texture.loadFromFile(IMAGE_DIR + "hearts.png");
        m_animation.Create("rotation", m_texture, 0, 0, 39, 32, 6, 8, 39, true);
    }
    else
    {
        throw std::logic_error("unknown bonus type");
    }

    m_position = position;
    m_bounds = bounds;
    m_type = type;

    m_animation.Set("rotation");
    m_animation.SetPosition(m_position);
}

void CBonus::OnContact(IContactHandler &entity)
{
    if (entity.GetType() == Entity::Runner)
    {
        m_health = 0;
    }
}

Entity CBonus::GetType() const
{
    return m_type;
}

void CBonus::OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object)
{
}

bool CBonus::IsAlive() const
{
    return (m_health > 0);
}

void CBonus::Update(const float elapsedTime)
{
    m_animation.Update(elapsedTime);
}

void CBonus::Draw(sf::RenderWindow &window) const
{
    m_animation.Draw(window);
}

sf::FloatRect CBonus::ToFloatRect() const
{
    return sf::FloatRect(m_position, m_bounds);
}

sf::Vector2f CBonus::GetMovementVector() const
{
    return sf::Vector2f(0, 0);
}

sf::Vector2f CBonus::GetPosition() const
{
    return m_position;
}
