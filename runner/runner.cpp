#include "stdafx.h"
#include "SpriteFile.h"
#include "constants.h"
#include "Animation.h"
#include "TmxLevel.h"
#include "ResourceHandler.h"
#include "ContactHandler.h"
#include "Enemy.h"
#include "Platform.h"
#include "Bullet.h"
#include "Runner.h"

std::string StateToString(const CRunner::State &state)
{
    std::string result;

    switch (state)
    {
    case CRunner::State::Stay:
        result = "stay";
        break;
    case CRunner::State::Walk:
        result = "walk";
        break;
    case CRunner::State::Jump:
        result = "jump";
        break;
    case CRunner::State::Climb:
        result = "climb";
        break;
    default:
        result = "<unknown state>";
        break;
    }

    return result;
}

CRunner::CRunner(CGameResources &resources)
    : m_res(resources)
{
    m_texture.loadFromFile(IMAGE_DIR + "runner.png");
    m_parachute.LoadFromFile(IMAGE_DIR + "parachute.png");
    m_texture.setSmooth(true);

    m_animation.Create("walk", m_texture, 0, 244, 40, 51, 6, 8, 40, true);
    m_animation.Create("stay", m_texture, 0, 187, 44, 51, 3, 6, 44, true);
    m_animation.Create("jump", m_texture, 0, 526, 36, 32, 4, 8, 36, true);

    m_animation.Create("shoot-stay", m_texture, 0, 572, 45, 52, 5, 8, 45, false);
    m_animation.Create("shoot-walk", m_texture, 0, 900, 45, 50, 5, 8, 50, false);
    m_animation.Create("shoot-jump", m_texture, 0, 1015, 36, 32, 4, 8, 36, false);

    m_animation.Create("climb-up", m_texture, 0, 368, 40, 63, 6, 8, 40, true);
    m_animation.Create("climb-down", m_texture, 0, 467, 35, 58, 6, 8, 35, true);

    m_animation.LoadFromXml(XML_DIR + "runner-animations.xml", m_texture);
    m_animation.SetScale(sf::Vector2f(1.2f, 1.2f));
}

CRunner::~CRunner() noexcept
{
    Clear();
}

void CRunner::Initialize(TmxLevel &level, unsigned score, unsigned health)
{
    m_objects = level.GetAllObjects();
    sf::FloatRect rect = level.GetObject("runner").rect;

    m_position = sf::Vector2f(rect.left, rect.top);
    m_speed = RUNNER_INITIAL_SPEED;

    m_directionX = DirectionX::Right;
    m_state = State::Stay;

    m_animation.Set("stay");
    m_bounds = sf::Vector2f(48, 60);
    m_movement = { 0, 0 };

    m_isAlive = true;

    m_health = health;
    m_score = score;

    m_animation.SetPosition(m_position);
}

void CRunner::Update(const float elapsedTime)
{
    if (m_movement.y > 1.f)
    {
        m_state = State::Jump;
    }

    // runner fell down
    if (m_position.y > (FIELD_HEIGHT * BLOCK_SIZE))
    {
        m_health = 0;
    }

    m_isAlive = (m_health > 0);
    m_elapsedTime = elapsedTime;

    if (m_isAttacked)
    {
        float timeSinceAttacked = m_clock.getElapsedTime().asSeconds();

        if (timeSinceAttacked > 0.5f)
        {
            m_clock.restart();
            m_isAttacked = false;
        }

        m_animation.SetColor(sf::Color::Red);
    }
    else
    {
        m_animation.SetColor(sf::Color::White);
        m_clock.restart();
    }

    if ((m_state == State::Climb) && (!m_isOnLadder))
    {
        m_state = State::Stay;
    }

    if (m_state != State::Climb)
    {
        m_movement.y += GRAVITY_ACCELERATION * elapsedTime;
    }

    if ((m_isParachuteOpened) && (m_movement.y > 0))
    {
        m_movement.y = 0.5f;
    }

    if (!m_isAttacked)
    {
        HandleKeyboardEvents();
    }

    UpdateAnimation(elapsedTime);
    UpdateBullets(elapsedTime);

    m_isOnLadder = false;
    m_isOnTopLadderBlock = false;
    m_isOnPlatform = false;
}

void CRunner::HandleKeyboardEvents()
{
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::D)))
    {
        if (m_state == State::Stay)
        {
            m_state = State::Walk;
        }

        m_directionX = DirectionX::Right;
        m_movement.x = 1;
    }
    else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::A)))
    {
        if (m_state == State::Stay)
        {
            m_state = State::Walk;
        }

        m_directionX = DirectionX::Left;
        m_movement.x = -1;
    }

    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::W)))
    {
        if (m_isOnLadder)
        {
            m_state = State::Climb;
        }

        if (((m_state == State::Stay) || (m_state == State::Walk)) && (fabs(m_movement.y <= 0.2f) || m_isOnPlatform))
        {
            m_movement.y = -3;
            m_state = State::Jump;
        }

        if (m_state == State::Climb)
        {
            m_movement.y = -0.5;
        }
    }

    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::S)))
    {
        if (m_isOnLadder)
        {
            m_state = State::Climb;
        }

        if (m_state == State::Climb)
        {
            m_movement.y = 0.5;
        }

        if (m_isOnTopLadderBlock)
        {
            m_movement.y = 0.5;
            m_state = State::Climb;
            m_isGoingDownOnLadder = true;
        }
    }
    else
    {
        m_isGoingDownOnLadder = false;
    }

    m_isParachuteOpened = sf::Keyboard::isKeyPressed(sf::Keyboard::E);

    if ((!sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) && (!sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) &&
        (!sf::Keyboard::isKeyPressed(sf::Keyboard::A)) && (!sf::Keyboard::isKeyPressed(sf::Keyboard::D)))
    {
        if (m_state == State::Walk)
        {
            m_state = State::Stay;
        }

        m_movement.x = 0;
    }

    if ((!sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) && (!sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) &&
        (!sf::Keyboard::isKeyPressed(sf::Keyboard::W)) && (!sf::Keyboard::isKeyPressed(sf::Keyboard::S)))
    {
        if (m_state == State::Climb)
        {
            m_movement.y = 0;
        }
    }

    // slowing movement by x axis if climbing on ladder
    if ((m_state == State::Climb) && (m_isOnLadder))
    {
        m_movement.x *= 0.25;
    }
}

void CRunner::UpdateAnimation(const float elapsedTime)
{
    switch (m_state)
    {
        case (State::Stay):
        {
            m_animation.Set("stay");
            break;
        }
        case (State::Walk):
        {
            m_animation.Set("walk");
            break;
        }
        case (State::Jump):
        {
            m_animation.Set("jump");
            break;
        }
        case (State::Climb):
        {
            if (m_movement.y > 0)
            {
                m_animation.Set("climb-down");
            }
            else
            {
                m_animation.Set("climb-up");
            }
            break;
        }
        default:
        {
            m_animation.Set("stay");
            break;
        }
    }

    if (m_isShooting)
    {
        if (m_state == State::Stay)
        {
            m_animation.Set("shoot-stay");
        }

        if (m_state == State::Walk)
        {
            m_animation.Set("shoot-walk");
        }

        if (m_state == State::Jump)
        {
            m_animation.Set("shoot-jump");
        }
    }

    if (!m_animation.IsPlaying())
    {
        m_isShooting = false;
        m_animation.Play();
    }

    if (GetDirectionX() == DirectionX::Left)
    {
        m_animation.Flip(true);
    }

    m_animation.Update(elapsedTime);
}

void CRunner::ShootBullet(TmxLevel &level)
{
    sf::Vector2f position;
    sf::Vector2f movement;

    float handOffsetX;
    float handOffsetY;

    if (GetDirectionX() == DirectionX::Left)
    {
        movement = sf::Vector2f(-1, 0);
        handOffsetX = 0.f;
        handOffsetY = 14.f;
    }
    else if (GetDirectionX() == DirectionX::Right)
    {
        movement = sf::Vector2f(1, 0);
        handOffsetX = 24.f;
        handOffsetY = 14.f;
    }

    position = m_position + sf::Vector2f(handOffsetX, handOffsetY);
    m_bullets.push_back(new CBullet(Entity::Runner, position, movement));

    m_isShooting = true;
}

void CRunner::UpdateBullets(const float elapsedTime)
{
    for (const auto &bullet : m_bullets)
    {
        bullet->Update(elapsedTime, m_objects);
    }

    auto isBulletDestroyed = [](CBullet *bullet)
    {
        if (!bullet->IsAlive())
        {
            delete bullet;
            return true;
        }

        return false;
    };

    m_bullets.erase(std::remove_if(m_bullets.begin(), m_bullets.end(), isBulletDestroyed), m_bullets.end());
}

void CRunner::MoveOnStep(const Axis axis, const float elapsedTime)
{
    const float step = m_speed * elapsedTime;

    if (axis == Axis::X)
    {
        m_position.x += m_movement.x * step;
    }
    else if (axis == Axis::Y)
    {
        m_position.y += m_movement.y * step;
    }

    m_animation.SetPosition(m_position);
    m_parachute.SetPosition(sf::Vector2f(m_position.x - 25, m_position.y - 90));
}

void CRunner::OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object)
{
    if (category == StaticCategory::Solid)
    {
        if ((m_movement.x < 0) && (axis == Axis::X))
        {
            m_position.x = object.left + object.width;
            m_movement.x = 0;
        }
        else if ((m_movement.x > 0) && (axis == Axis::X))
        {
            m_position.x = object.left - m_bounds.x;
            m_movement.x = 0;
        }

        if ((m_movement.y < 0) && (axis == Axis::Y))
        {
            m_position.y = object.top + object.height;
            m_movement.y = 0;
        }
        else if ((m_movement.y > 0) && (axis == Axis::Y))
        {
            m_state = State::Stay; // standing on ground
            m_position.y = object.top - m_bounds.y;
            m_movement.y = 0;
        }
    }

    if (category == StaticCategory::SolidFromTop)
    {
        if ((m_movement.y > 0) && (axis == Axis::Y) && (!m_isGoingDownOnLadder))
        {
            m_state = State::Stay; // standing on ground
            m_position.y = object.top - m_bounds.y;
            m_movement.y = 0;
        }
    }

    if (category == StaticCategory::SlopeLeft)
    {
        const float tangent = object.height / object.width;
        const float offsetX = m_position.x + (m_bounds.x / 2) - object.left;
        const float offsetY = object.top - m_bounds.y;

        const float newPosY = offsetX * tangent + offsetY;

        if ((m_position.y > newPosY) && ((m_bounds.x / 2) + m_position.x > object.left) && (m_movement.y > 0))
        {
            m_state = State::Stay; // standing on ground
            m_position.y = newPosY;
            m_movement.y = 0;
        }
    }

    if (category == StaticCategory::SlopeRight)
    {
        const float tangent = object.height / object.width;
        const float offsetX = m_position.x + (object.width / 2) - object.left;
        const float offsetY = object.top + object.height - m_bounds.y;

        const float newPosY = -offsetX * tangent + offsetY;

        if ((m_position.y > newPosY) && (m_position.x + (object.width / 2) < object.left + object.width) && (m_movement.y > 0))
        {
            m_state = State::Stay; // standing on ground
            m_position.y = newPosY;
            m_movement.y = 0;
        }
    }

    if ((category == StaticCategory::Spike) && (!IsAttacked()))
    {
        m_isAttacked = true;
        m_movement.y = -2.5f;
        m_movement.x = 0.1f;
        --m_health;
        m_res.sounds.at(Sound::RunnerDamage).Play();
    }

    if (category == StaticCategory::Ladder)
    {
        m_isOnLadder = true;
    }

    if (category == StaticCategory::TopLadderBlock)
    {
        m_isOnTopLadderBlock = true;
    }
}

void CRunner::Draw(sf::RenderTarget &target) const
{
    if ((m_isParachuteOpened) && (m_movement.y > 0))
    {
        m_parachute.Draw(target);
    }

    m_animation.Draw(target);

    for (const auto &bullet : m_bullets)
    {
        bullet->Draw(target);
    }
}

void CRunner::OnContact(IContactHandler &entity)
{
    if (entity.GetType() == Entity::Coin)
    {
        ++m_score;
    }

    if (entity.GetType() == Entity::Heart)
    {
        ++m_health;
    }

    if ((entity.GetType() == Entity::Enemy) || (entity.GetType() == Entity::Cannon) || (entity.GetType() == Entity::Bullet))
    {
        if (entity.GetType() == Entity::Bullet)
        {
            m_res.sounds.at(Sound::RunnerDamage).Play();
        }

        // ������ �� ��� ������ ���� � ������� n ������
        if (!m_isAttacked)
        {
            --m_health;

            m_movement.y = -3.f;
            m_movement.x = 0.8f;

            // ����� ��������� ����� �� �����
            if (ToFloatRect().left < entity.ToFloatRect().left)
            {
                m_movement.x *= -1;
            }

            m_state = State::Jump;
            m_isAttacked = true;
        }
    }

    if (entity.GetType() == Entity::Platform)
    {
        const float offsetX = 10;

        // if runner collides with platform from left
        if (m_position.x + m_bounds.x - offsetX < entity.ToFloatRect().left)
        {
            m_position.x = entity.ToFloatRect().left - m_bounds.x;
            return;
        } // from right
        else if (m_position.x + offsetX > entity.ToFloatRect().left + entity.ToFloatRect().width)
        {
            m_position.x = entity.ToFloatRect().left + entity.ToFloatRect().width;
            return;
        }

        // from up
        if (m_movement.y > 0)
        {
            m_isOnPlatform = true;
            m_movement.x = entity.GetMovementVector().x;
            m_movement.y = entity.GetMovementVector().y;

            if (m_movement.y < 0)
            {
                m_movement.y = 0;
            }

            m_position.y = entity.ToFloatRect().top - m_bounds.y;
            m_position.x += (m_movement.x * PLATFORM_SPEED * m_elapsedTime);
            m_state = State::Stay;
            return;
        } // from bottom
        else
        {
            if (entity.GetMovementVector().y > 0)
            {
                m_position.y = entity.ToFloatRect().top + entity.ToFloatRect().height + 10;
            }
            else
            {
                m_position.y = entity.ToFloatRect().top + entity.ToFloatRect().height;
            }
            m_movement.y = 0;
            return;
        }
    }
}

float CRunner::GetSpeed() const
{
    return m_speed;
}

int CRunner::GetScore() const
{
    return m_score;
}

int CRunner::GetHealth() const
{
    return m_health;
}

sf::FloatRect CRunner::ToFloatRect() const
{
    return sf::FloatRect(m_position, m_bounds);
}

sf::Vector2f CRunner::GetPosition() const
{
    return m_animation.GetPosition();
}

CRunner::State CRunner::GetState() const
{
    return m_state;
}

std::vector<CBullet*> CRunner::GetBullets() const
{
    return m_bullets;
}

Entity CRunner::GetType() const
{
    return Entity::Runner;
}

DirectionX CRunner::GetDirectionX() const
{
    return m_directionX;
}

DirectionY CRunner::GetDirectionY() const
{
    return (m_movement.y >= 0) ? DirectionY::Down : DirectionY::Up;
}

sf::Vector2f CRunner::GetMovementVector() const
{
    return m_movement;
}

bool CRunner::IsAttacked() const
{
    return m_isAttacked;
}

bool CRunner::IsAlive() const
{
    return m_isAlive;
}

void CRunner::Clear()
{
    m_objects.clear();
    m_bullets.clear();
}
