#include "stdafx.h"
#include "constants.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

const char* WINDOW_TITLE = "Runner";

const std::string CONTENT_DIR = "resources/";

const std::string SOUND_DIR = CONTENT_DIR + "sounds/";
const std::string MUSIC_DIR = CONTENT_DIR + "musics/";
const std::string IMAGE_DIR = CONTENT_DIR + "images/";
const std::string FONTS_DIR = CONTENT_DIR + "fonts/";
const std::string XML_DIR = CONTENT_DIR + "xml/";

const char* const MENU_ITEMS_INITIAL[] = {"Play", "Options", "Exit"};
const char* const MENU_ITEMS_ON_GAME_PAUSE[] = {"Resume", "Restart", "Options", "Exit"};
const char* const MENU_ITEMS_ON_GAME_LOSS[] = {"Try again", "Exit"};
const char* const MENU_ITEMS_ON_GAME_WIN[] = {"Restart", "Exit"};

const unsigned int FIELD_WIDTH = 140;
const unsigned int FIELD_HEIGHT = 40;

const float BLOCK_SIZE = 32.f;

const float RUNNER_INITIAL_SPEED = 220.f;
const float BULLET_SPEED = 400.f;
const float PLATFORM_SPEED = 85.f;
const float TEXTBOX_MOVEMENT_SPEED = 40.f;

const float GRAVITY_ACCELERATION = 7.1f;

const sf::Color SKY_COLOR = sf::Color(117, 187, 253);
