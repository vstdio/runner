#pragma once

#include "Animation.h"
#include "ContactHandler.h"
#include "TmxLevel.h"

class CBullet final : public IContactHandler
{
public:
    CBullet(Entity type, const sf::Vector2f &position, const sf::Vector2f &movement);
    ~CBullet() = default;

    // void Initialize(const sf::Vector2f &position, const sf::Vector2f &movement);
    void Update(const float elapsedTime, const std::vector<TmxObject> &objects);
    void Draw(sf::RenderTarget &target);

    bool IsAlive() const;

    void OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object);
    void OnContact(IContactHandler &entity);
    Entity GetType() const;
    sf::FloatRect ToFloatRect() const;

    DirectionX GetDirectionX() const;
    DirectionY GetDirectionY() const;

    sf::Vector2f GetMovementVector() const;
    sf::Vector2f GetPosition() const;

    bool IsAbleToHurt() const;

private:
    sf::Texture m_texture;
    CAnimation m_animation;

    sf::Vector2f m_position;
    sf::Vector2f m_movement;

    float m_speed = BULLET_SPEED;
    int m_health = 100;
    bool m_isAlive = true;
};
