#include "stdafx.h"
#include "constants.h"
#include "Platform.h"

CPlatform::CPlatform(TmxLevel &level, const sf::Vector2f &movement, const sf::Vector2f &position, const sf::Vector2f &bounds)
    : m_position(position)
    , m_bounds(bounds)
    , m_movement(movement)
{
    m_texture.loadFromFile("resources/images/platform.png");

    m_animation.Create("move", m_texture, 0, 0, 95, 22, 1, 0, 95, true);
    m_animation.Set("move");

    m_objects = level.GetObjects("platform-solid");

    m_directionX = (m_movement.x >= 0) ? (DirectionX::Right) : (DirectionX::Left);
    m_directionY = (m_movement.y >= 0) ? (DirectionY::Down) : (DirectionY::Up);
}

void CPlatform::Update(const float elapsedTime)
{
    const float step = PLATFORM_SPEED * elapsedTime;
    m_position += m_movement * step;

    for (auto &object : m_objects)
    {
        if (ToFloatRect().intersects(object.rect))
        {
            if ((object.type == "left") && (GetDirectionX() == DirectionX::Left))
            {
                m_movement.x = 1;
                m_directionX = DirectionX::Right;
            }
            else if ((object.type == "right") && (GetDirectionX() == DirectionX::Right))
            {
                m_movement.x = -1;
                m_directionX = DirectionX::Left;
            }
            else if ((object.type == "up") && (GetDirectionY() == DirectionY::Up))
            {
                m_movement.y = 1;
                m_directionY = DirectionY::Down;
            }
            else if ((object.type == "down") && (GetDirectionY() == DirectionY::Down))
            {
                m_movement.y = -1;
                m_directionY = DirectionY::Up;
            }

            break;
        }
    }

    m_animation.Update(elapsedTime);
    m_animation.SetPosition(m_position);
}

void CPlatform::OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object)
{
}

void CPlatform::Draw(sf::RenderWindow &window) const
{
    m_animation.Draw(window);
}

sf::FloatRect CPlatform::ToFloatRect() const
{
    return sf::FloatRect(m_position, m_bounds);
}

sf::Vector2f CPlatform::GetMovementVector() const
{
    return m_movement;
}

sf::Vector2f CPlatform::GetPosition() const
{
    return m_position;
}

void CPlatform::OnContact(IContactHandler &entity)
{
}

Entity CPlatform::GetType() const
{
    return Entity::Platform;
}

DirectionX CPlatform::GetDirectionX() const
{
    return m_directionX;
}

DirectionY CPlatform::GetDirectionY() const
{
    return m_directionY;
}
