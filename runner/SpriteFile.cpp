#include "stdafx.h"
#include "SpriteFile.h"

void CSpriteFile::LoadFromFile(const std::string &fileName)
{
    if (!m_texture.loadFromFile(fileName))
    {
        throw std::runtime_error("failed to load texture from " + fileName);
    }

    m_sprite.setTexture(m_texture);
}

void CSpriteFile::SetTexture(const sf::Texture &texture)
{
    m_sprite.setTexture(texture);
}

void CSpriteFile::SetPosition(const sf::Vector2f &position)
{
    m_sprite.setPosition(position);
}

void CSpriteFile::SetScale(const sf::Vector2f &scale)
{
    m_sprite.setScale(scale);
}

void CSpriteFile::SetTextureRect(const sf::IntRect &rect)
{
    m_sprite.setTextureRect(rect);
}

void CSpriteFile::ResizeToTargetSize(sf::RenderWindow &target)
{
    SetScale(sf::Vector2f(1.0f, 1.0f));

    const float x = target.getSize().x / ToFloatRect().width;
    const float y = target.getSize().y / ToFloatRect().height;

    SetScale(sf::Vector2f(x, y));
}

sf::FloatRect CSpriteFile::ToFloatRect() const
{
    return m_sprite.getGlobalBounds();
}

void CSpriteFile::Draw(sf::RenderTarget &target) const
{
    target.draw(m_sprite);
}

sf::Vector2f CSpriteFile::GetScale() const
{
    return m_sprite.getScale();
}

void CSpriteFile::SetColor(const sf::Color &color)
{
    m_sprite.setColor(color);
}

sf::Vector2f CSpriteFile::GetOrigin() const
{
    return m_sprite.getOrigin();
}

sf::Vector2f CSpriteFile::GetPosition() const
{
    return m_sprite.getPosition();
}

void CSpriteFile::SetOrigin(const sf::Vector2f &origin)
{
    m_sprite.setOrigin(origin);
}

void CSpriteFile::SetRotation(float angle)
{
    m_sprite.setRotation(angle);
}
