#include "stdafx.h"
#include "constants.h"
#include "Bullet.h"
#include "Enemy.h"

CEnemy::CEnemy(TmxLevel &level, Entity entity, const sf::Vector2f &position, const sf::Vector2f &bounds)
    : m_position(position)
    , m_bounds(bounds)
    , m_healthBar(CHealthBar(sf::Vector2f(50, 8), 3))
    , m_state(State::Move)
    , m_type(entity)
{
    switch (entity)
    {
    case Entity::Enemy:
        m_texture.loadFromFile(IMAGE_DIR + "enemy.png");
        m_animation.Create("walk", m_texture, 0, 0, 50, 80, 1, 10, 50, true);
        m_movement = sf::Vector2f(1, 0);
        break;
    case Entity::Cannon:
        m_texture.loadFromFile(IMAGE_DIR + "cannon.png");
        m_animation.Create("walk", m_texture, 0, 0, 82, 53, 1, 10, 82, true);
        m_movement = sf::Vector2f(0, 0);
        break;
    default:
        throw std::logic_error("invalid enemy type");
        break;
    }

    m_bulletTimer.restart();
    m_status = Status::Idle;
    m_objects = level.GetObjects("enemy-solid");
}

void CEnemy::Draw(sf::RenderTarget &target) const
{
    if ((m_isAttacked) && (m_state != State::Fall))
    {
        m_healthBar.Draw(target);
    }

    m_animation.Draw(target);
    DrawBullets(target);
}

void CEnemy::Update(const float elapsedTime, const IContactHandler &entity, const CGameResources &res)
{
    if (m_state == State::Move)
    {
        ProcessContactTimeEvents();
        UpdatePosition(elapsedTime);
        UpdateAnimation(elapsedTime);
        ProcessCollisions();
        UpdateState(entity);
        UpdateBullets();

        if ((m_status == Status::Pursuit) && (GetType() == Entity::Cannon))
        {
            ShootBullet(res);
        }
    }
    else if (m_state == State::Fall)
    {
        UpdatePosition(elapsedTime);
        Rotate(elapsedTime);
        UpdateState(entity);
    }
}

void CEnemy::ShootBullet(const CGameResources &res)
{
    if (m_bulletTimer.getElapsedTime().asSeconds() >= 1.f)
    {
        res.sounds.at(Sound::CannonShoot).Play();
        m_bulletTimer.restart();
        m_bullets.push_back(new CBullet(Entity::Cannon, sf::Vector2f(m_position.x - 20, m_position.y + 10), sf::Vector2f(-1, 0)));
    }
}

void CEnemy::UpdateBullets()
{
    auto isBulletDestroyed = [](CBullet *bullet)
    {
        if (!bullet->IsAlive())
        {
            delete bullet;
            return true;
        }

        return false;
    };

    m_bullets.erase(std::remove_if(m_bullets.begin(), m_bullets.end(), isBulletDestroyed), m_bullets.end());
}

void CEnemy::DrawBullets(sf::RenderTarget &target) const
{
    for (const auto &bullet : m_bullets)
    {
        bullet->Draw(target);
    }
}

void CEnemy::UpdatePosition(const float elapsedTime)
{
    const float step = m_speed * elapsedTime;

    if (m_state == State::Move)
    {
        m_position += m_movement * step;
        UpdateHealthBarPosition();
    }
    else if (m_state == State::Fall)
    {
        m_position.y += step;
    }

    m_animation.SetPosition(m_position);
}

void CEnemy::UpdateAnimation(const float elapsedTime)
{
    m_animation.Set("walk");

    if (GetDirectionX() == DirectionX::Left)
    {
        m_animation.Flip(true);
    }

    m_animation.Update(elapsedTime);
}

void CEnemy::UpdateHealthBarPosition()
{
    const float x = m_position.x + (m_animation.GetWidth() / 2.f) - (m_healthBar.GetSize().x / 2.f);
    const float y = m_position.y - (2.f * m_healthBar.GetSize().y);

    m_healthBar.SetPosition(sf::Vector2f(x, y));
}

void CEnemy::UpdateState(const IContactHandler &entity)
{
    if (GetType() == Entity::Cannon)
    {
        const sf::FloatRect area = sf::FloatRect(m_position.x - 400, m_position.y, 400, 30);

        if (area.intersects(entity.ToFloatRect()))
        {
            m_status = Status::Pursuit;
        }
        else
        {
            m_status = Status::Idle;
        }
    }

    m_state = (m_health > 0) ? State::Move : State::Fall;
    m_isAlive = (m_angle < 45.f);
}

void CEnemy::Rotate(const float elapsedTime)
{
    m_animation.SetOrigin(sf::Vector2f(m_animation.GetGlobalBounds().width / 2, m_animation.GetGlobalBounds().height / 2));
    m_angle += m_speed * elapsedTime;

    if (m_directionX == DirectionX::Left)
    {
        m_animation.SetRotation(m_angle);
    }
    else if (m_directionX == DirectionX::Right)
    {
        m_animation.SetRotation(-m_angle);
    }

    m_animation.SetOrigin(sf::Vector2f(0, 0));
}

void CEnemy::ProcessCollisions()
{
    for (size_t i = 0; i < m_objects.size(); ++i)
    {
        TmxObject object = m_objects[i];
        sf::FloatRect rect = object.rect;

        if (ToFloatRect().intersects(rect))
        {
            if (object.name == "enemy-solid")
            {
                if ((object.type == "left") && (GetDirectionX() == DirectionX::Left))
                {
                    m_movement.x = 1;
                    m_directionX = DirectionX::Right;
                }
                else if ((object.type == "right") && (GetDirectionX() == DirectionX::Right))
                {
                    m_movement.x = -1;
                    m_directionX = DirectionX::Left;
                }
            }
        }
    }
}

void CEnemy::OnContact(IContactHandler &entity)
{
    if ((entity.GetType() == Entity::Bullet) && (!m_contactedOnce))
    {
        --m_health;
        m_speed = 70.f;
        m_healthBar.DecreaseHealth();
        m_contactedOnce = true;
        m_isAttacked = true;
    }

    if (entity.GetType() == Entity::Runner)
    {
        entity.OnContact(*this);
    }
}

void CEnemy::ProcessContactTimeEvents()
{
    if ((m_contactedOnce) && (m_clock.getElapsedTime().asSeconds() > 0.5f))
    {
        m_clock.restart();
        m_contactedOnce = false;
    }
    else if (!m_contactedOnce)
    {
        m_speed = 100.f;
        m_clock.restart();
    }
}

void CEnemy::OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object)
{
}

std::vector<CBullet*> CEnemy::GetBullets() const
{
    return m_bullets;
}

sf::FloatRect CEnemy::ToFloatRect() const
{
    return sf::FloatRect(m_position, m_bounds);
}

Entity CEnemy::GetType() const
{
    return m_type;
}

bool CEnemy::IsAlive() const
{
    return m_isAlive;
}

bool CEnemy::IsAbleToHurt() const
{
    return (m_state == State::Move);
}

DirectionX CEnemy::GetDirectionX() const
{
    return m_directionX;
}

DirectionY CEnemy::GetDirectionY() const
{
    return m_directionY;
}

sf::Vector2f CEnemy::GetMovementVector() const
{
    return m_movement;
}

sf::Vector2f CEnemy::GetPosition() const
{
    return m_position;
}
