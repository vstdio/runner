#pragma once

#include "ContactHandler.h"
#include "Animation.h"
#include "Bullet.h"
#include "HealthBar.h"
#include "ResourceHandler.h"
#include "TmxLevel.h"

class CEnemy final : public IContactHandler
{
    enum State
    {
        Fall,
        Move
    };

    enum Status
    {
        Idle,
        Pursuit
    };

public:
    CEnemy(TmxLevel &level, Entity entity, const sf::Vector2f &position, const sf::Vector2f &bounds);
    ~CEnemy() = default;

    void Update(const float elapsedTime, const IContactHandler &entity, const CGameResources &res);
    void Draw(sf::RenderTarget &target) const;

    void OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object);
    void OnContact(IContactHandler &entity);
    sf::FloatRect ToFloatRect() const;
    Entity GetType() const;

    DirectionX GetDirectionX() const;
    DirectionY GetDirectionY() const;

    sf::Vector2f GetMovementVector() const;
    sf::Vector2f GetPosition() const;

    bool IsAlive() const;
    bool IsAbleToHurt() const;

    std::vector<CBullet*> GetBullets() const;

private:
    void ProcessCollisions();
    void ProcessContactTimeEvents();

    void UpdatePosition(const float elapsedTime);
    void UpdateAnimation(const float elapsedTime);
    void UpdateState(const IContactHandler &entity);

    void Rotate(const float elapsedTime);
    void UpdateHealthBarPosition();

    void ShootBullet(const CGameResources &res);
    void UpdateBullets();
    void DrawBullets(sf::RenderTarget &target) const;

    sf::Texture m_texture;
    CAnimation m_animation;

    sf::Vector2f m_position;
    sf::Vector2f m_movement;
    sf::Vector2f m_bounds;

    CHealthBar m_healthBar;
    sf::Clock m_clock;
    sf::Clock m_bulletTimer;

    std::vector<TmxObject> m_objects;
    std::vector<CBullet*> m_bullets;

    DirectionX m_directionX = DirectionX::Right;
    DirectionY m_directionY = DirectionY::Up;

    float m_speed = 100.f;
    float m_angle = 0.f;
    int m_health = 3;

    State m_state;
    Status m_status;

    Entity m_type;

    bool m_isAlive = true;
    bool m_contactedOnce = false;
    bool m_isAttacked = false;
    bool m_isAbleToHurt = false;
};
