#pragma once

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

extern const char* WINDOW_TITLE;

extern const std::string CONTENT_DIR;

extern const std::string SOUND_DIR;
extern const std::string MUSIC_DIR;
extern const std::string IMAGE_DIR;
extern const std::string FONTS_DIR;
extern const std::string XML_DIR;

extern const char* const MENU_ITEMS_INITIAL[];
extern const char* const MENU_ITEMS_ON_GAME_PAUSE[];
extern const char* const MENU_ITEMS_ON_GAME_LOSS[];
extern const char* const MENU_ITEMS_ON_GAME_WIN[];

extern const unsigned int FIELD_WIDTH;
extern const unsigned int FIELD_HEIGHT;
extern const float BLOCK_SIZE;

extern const float RUNNER_INITIAL_SPEED;
extern const float BULLET_SPEED;
extern const float PLATFORM_SPEED;
extern const float TEXTBOX_MOVEMENT_SPEED;

extern const float GRAVITY_ACCELERATION;

extern const sf::Color SKY_COLOR;
