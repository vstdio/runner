#pragma once

#ifndef LEVEL_H
#define LEVEL_H

#include "sdk/TinyXML/tinyxml.h"

struct TmxObject
{
    int GetPropertyInt(const std::string &name);
    float GetPropertyFloat(const std::string &name);
    std::string GetPropertyString(const std::string &name);

    std::string name;
    std::string type;
    sf::Rect<float> rect;
    std::map<std::string, std::string> properties;

    sf::Sprite sprite;
};

struct Layer
{
    int opacity;
    std::vector<sf::Sprite> tiles;
};

class TmxLevel
{
public:
    bool LoadFromFile(const std::string &filename);//���������� false ���� �� ���������� ���������
    TmxObject GetObject(const std::string &name);
    std::vector<TmxObject> GetObjects(const std::string &name);//������ ������ � ��� �������
    std::vector<TmxObject> GetAllObjects();//������ ��� ������� � ��� �������
    void Draw(sf::RenderTarget &target) const;
    sf::Vector2i GetTileSize();//�������� ������ �����
    // void Clear();

private:
    int width, height, tileWidth, tileHeight;//� tmx ����� width height � ������,����� ������ �����
    int firstTileID;//�������� ���� ������� �����
    sf::Rect<float> drawingBounds;//������ ����� ����� ������� ������
    sf::Texture tilesetImage;//�������� �����
    std::vector<TmxObject> objects;//������ ���� �������, ������� �� �������
    std::vector<Layer> layers;
};

#endif