#pragma once

#include "Animation.h"
#include "TmxLevel.h"
#include "ContactHandler.h"

class CPlatform final : public IContactHandler
{
public:
    CPlatform(TmxLevel &level, const sf::Vector2f &movement, const sf::Vector2f &position, const sf::Vector2f &bounds);
    ~CPlatform() = default;

    void Update(const float elapsedTime);
    void Draw(sf::RenderWindow &window) const;

    sf::Vector2f GetMovementVector() const;
    sf::Vector2f GetPosition() const;

    void OnContactStatic(Axis axis, StaticCategory category, const sf::FloatRect &object);
    void OnContact(IContactHandler &entity);
    sf::FloatRect ToFloatRect() const;
    Entity GetType() const;

    DirectionX GetDirectionX() const;
    DirectionY GetDirectionY() const;

private:
    sf::Texture m_texture;
    CAnimation m_animation;

    sf::Vector2f m_position;
    sf::Vector2f m_movement;
    sf::Vector2f m_bounds;

    DirectionX m_directionX = DirectionX::None;
    DirectionY m_directionY = DirectionY::None;

    std::vector<TmxObject> m_objects;
};
