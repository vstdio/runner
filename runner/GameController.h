#pragma once

class CGameController
{
    enum class AppState
    {
        Playing,
        InMenu,
        Loading
    };

public:
    CGameController();
    void EnterGameLoop();

private:
    void InitializeGameEntities();
    void InitializeNewGame();
    void InitializeMenu();

    void OpenWindow();

    void ProcessGameUserInputEvents(const sf::Event &event);
    void ProcessMenuUserInputEvents(const sf::Event &event);
    void ProcessItemPressInMenu();

    void HandleEvents();
    void Update();
    void Render();

    void ProcessWindowResizing(const sf::Event &event);
    void MoveScreenViewByRunnerPosition();
    void SetScreenViewToDefault();

    void ProcessCollisions(IContactHandler &entity, const Axis axis);
    bool HandleStaticIntersection(IContactHandler &entity, StaticCategory category);

    void UpdateRunner();

    void UpdatePlatforms();
    void DrawPlatforms();

    void UpdateBonuses();
    void DrawBonuses();

    void UpdateEnemies();
    void DrawEnemies();

    void UpdateTextBoxes();
    void DrawTextBoxes();

    void UpdateCursor(const sf::Event &event);
    float GetFps() const;

    void DisableVolume();
    void EnableVolume();

    void LevelRawMemoryRealloc(unsigned levelNo);

    sf::RenderWindow m_window;
    sf::Clock m_clock;
    sf::View m_view;

    CGameMenu m_menu;
    CGameResources m_assets;

    CRunner m_runner;
    AppState m_state;

    TmxLevel *m_level;
    std::vector<TmxObject> m_tmxObjects;

    std::vector<CEnemy*> m_enemies;
    std::vector<CPlatform*> m_platforms;
    std::vector<CBonus*> m_bonuses;
    std::vector<CTextBox*> m_textBoxes;

    float m_elapsedTime;
    unsigned m_levelNo = 1;
    bool m_levelWasLoaded = false;
};
